package quarto;
import java.util.Enumeration;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.util.ArrayList;
import javax.swing.JOptionPane;
//import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

/**********************************************************
* The class gives the parameters on ways to start the game.
* It handles any new events performed in the game.
* @author Gloire Rubambiza
* @since 12/06/2016
***********************************************************/
public final class StartGame extends JPanel implements ActionListener {
	
	/** Grid layout used for buttons panel. */
	private static final GridLayout GRID_LAYOUT = new GridLayout(1, 3, 300, 0);

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** Radio buttons for selecting players options when setting up a game. */
	private JRadioButton player1Human, player2Human, player1AI, player2AI;
	
	/** The buttons to be clicked when setting up a new game. */
	private JButton ok, cancel;
	
	/** Button for info on the game. */
	private JButton about;
	
	/** Set the different AI levels for players. */
	private JRadioButton[] aiLevelsOne;
	
	/** Set the different AI levels for players. */
	private JRadioButton[] aiLevelsTwo;
	
	/** Button groups for the AI level options. */
	private ButtonGroup playerOneAIGroup, playerTwoAIGroup;
	
	/** Panel for the aiLevels buttons. */
	//private JPanel aiLevelPanelOne, aiLevelPanelTwo;
	private JPanel player1Options, player2Options, buttonsPanel;
	
	/** Labels for the AI level buttons. */
	private JLabel aiLevelOneLabel, aiLevelTwoLabel, player1, player2;
	
	/** Holds the textfields for player inputs. */
	private JTextField playerOneText, playerTwoText;
	
	/** Button groups for the player options. */
	private ButtonGroup playerOneGroup, playerTwoGroup;
	
	/** Holds the label for beginning panel. */
	private JLabel playerOneStartLabel, playerTwoStartLabel;
	
	/** The parent frame of this panel. */
	private JFrame parent;
	
	/** Text field size. */
	private static final int TEXTFIELD_SIZE = 25;
	
	/** Beginning frame size width. */
	private static final int FRAME_WIDTH = 200;
	
	/** Beginning frame size height. */
	private static final int FRAME_HEIGHT = 300;
	
	/** Grid layout height for beginning frame. */
	private static final int GRID_HEIGHT = 6;
	
	/** The blue color used for most of the backgrounds in the game. */
	private static final java.awt.Color LAKER_BLUE = 
			new java.awt.Color(0, 101, 164);
	
	/** Lighter blue color for the inner functions. */
	private static final java.awt.Color LIGHT_BLUE = 
			new java.awt.Color(200, 220, 255);
	
	/** The font for most text in the GUI. */
	private static final Font NORMAL_FONT = 
			new Font("Cooper Black", Font.PLAIN, 20);
	
	/** Creates a padding around the main panel. */
	private static final Border CREATE_EMPTY_BORDER =
			BorderFactory.createEmptyBorder(40, 50, 40, 50);

	/** Creates a smaller padding around inner panels. */
	private static final Border ADD_SMALL_MARGIN =
			BorderFactory.createEmptyBorder(10, 50, 10, 50);
	
	/** Adds a vertical padding between elements inside inner panels. */
	private static final Border ADD_VERT_MARGIN = 
			BorderFactory.createEmptyBorder(10, 0, 10, 0);
	
	/** The layout for player1Options panel. */
	private BoxLayout box1;
	
	/** The layout for player2Options panel. */
	private BoxLayout box2;
	
	/************************************
	 * Constructor for the options panel.
	 * @param pParent is the parent frame of this panel.
	*************************************/
	public StartGame(final JFrame pParent) {
		this.parent = pParent;
		
		createComponents();
		
		styleComponents();
		
		setMainPanel();
	
	}
	
	/******************************************************
	 * Instantiates all the buttons, labels and text fields.
	 ******************************************************/
	private void createComponents() {
		ok = new JButton("OK");
		cancel = new JButton("CANCEL");
		about = new JButton("ABOUT QUARTO");
		ok.addActionListener(this);
		cancel.addActionListener(this);
		about.addActionListener(this);
		
		player1Options = new JPanel();
		player2Options = new JPanel();
		buttonsPanel = new JPanel();
		
		box1  = new BoxLayout(player1Options, BoxLayout.Y_AXIS);
		box2  = new BoxLayout(player2Options, BoxLayout.Y_AXIS);
		
		player1Options.setLayout(box1);
		player2Options.setLayout(box2);
		buttonsPanel.setLayout(GRID_LAYOUT);
		
		this.setLayout(new BorderLayout());
		
		player1 = new JLabel("Player 1");
		player1Human = new JRadioButton("Human");
		player1AI = new JRadioButton("AI");
		playerOneGroup = new ButtonGroup();
		playerOneGroup.add(player1Human);
		playerOneGroup.add(player1AI);
		aiLevelOneLabel = new JLabel("AI Difficulty");
		
		player1Options.add(player1);
		player1Options.add(player1Human);
		player1Options.add(player1AI);
		player1Options.add(aiLevelOneLabel);
		
		playerOneAIGroup = new ButtonGroup();
		aiLevelsOne = new JRadioButton[GRID_HEIGHT];
		
		player2 = new JLabel("Player 2");
		player2Human = new JRadioButton("Human");
		player2AI = new JRadioButton("AI");
		playerTwoGroup = new ButtonGroup();
		playerTwoGroup.add(player2Human);
		playerTwoGroup.add(player2AI);
		aiLevelTwoLabel = new JLabel("AI Difficulty");
		
		player2Options.add(player2);
		player2Options.add(player2Human);
		player2Options.add(player2AI);
		player2Options.add(aiLevelTwoLabel);
		
		playerTwoAIGroup = new ButtonGroup();
		aiLevelsTwo = new JRadioButton[GRID_HEIGHT];
		
		for (int i = 0; i < GRID_HEIGHT; ++i) {
			aiLevelsOne[i] = new JRadioButton("" + (i + 1));
			if (i == 0) {
				aiLevelsOne[i].setSelected(true);
			}
			aiLevelsOne[i].setFont(NORMAL_FONT);
			playerOneAIGroup.add(aiLevelsOne[i]);
			player1Options.add(aiLevelsOne[i]);
			
			// Create buttons for the second panel simultaneously
			aiLevelsTwo[i] = new JRadioButton("" + (i + 1));
			if (i == 0) {
				aiLevelsTwo[i].setSelected(true);
			}
			aiLevelsTwo[i].setFont(NORMAL_FONT);
			playerTwoAIGroup.add(aiLevelsTwo[i]);
			player2Options.add(aiLevelsTwo[i]);
			
		}
		
		playerOneText = new JTextField("Han Solo", TEXTFIELD_SIZE);
		playerTwoText = new JTextField("Darth Vader", TEXTFIELD_SIZE);
		playerOneStartLabel = new JLabel("Name: ");
		playerTwoStartLabel = new JLabel("Name: ");
		
		player1Options.add(playerOneStartLabel);
		player1Options.add(playerOneText);
		
		player2Options.add(playerTwoStartLabel);
		player2Options.add(playerTwoText);
		
		//buttonsPanel.add(errorsLabel, BorderLayout.NORTH);
		buttonsPanel.add(ok);
		buttonsPanel.add(about);
		buttonsPanel.add(cancel);
	
	}
	
	/*******************************************
	 * Adds all the components to the main panel.
	 *******************************************/
	private void setMainPanel() {
		setLayout(new BorderLayout());
		add(player1Options, BorderLayout.WEST);
		add(player2Options, BorderLayout.EAST);
		add(buttonsPanel, BorderLayout.SOUTH);
//		add(player1Human);
//		add(player1AI);
//		add(player2Human);
//		add(player2AI);
//		add(aiLevelPanelOne);
//		add(aiLevelPanelTwo);
//		add(playerOneStartLabel);
//		add(playerOneText);
//		add(playerTwoStartLabel);
//		add(playerTwoText);
//		add(ok);
//		add(cancel);
				
		setSize(FRAME_HEIGHT, FRAME_WIDTH);
		//setBackground(LAKER_BLUE);
		setVisible(true);
	}
	
	/********************************************************
	 * Sets all style for the buttons and labels on the panel.
	 ********************************************************/
	private void styleComponents() {
		this.setBackground(LAKER_BLUE);
		this.setBorder(CREATE_EMPTY_BORDER);
		
		player1Human.setSelected(true);
		player2Human.setSelected(true);
		
		for (Component c: this.getComponents()) {
			c.setFont(NORMAL_FONT);
			c.setBackground(LAKER_BLUE);
		}
		player1Options.setBackground(LIGHT_BLUE);
		player1Options.setBorder(ADD_SMALL_MARGIN);
		for (Component c: player1Options.getComponents()) {
			c.setFont(NORMAL_FONT);
			c.setBackground(LIGHT_BLUE);
			((JComponent) c).setBorder(ADD_VERT_MARGIN);
		}
		player2Options.setBackground(LIGHT_BLUE);
		player2Options.setBorder(ADD_SMALL_MARGIN);
		for (Component c: player2Options.getComponents()) {
			c.setFont(NORMAL_FONT);
			c.setBackground(LIGHT_BLUE);
			((JComponent) c).setBorder(ADD_VERT_MARGIN);
			((JComponent) c).setAlignmentX(Component.RIGHT_ALIGNMENT);
			if (c instanceof JTextField) {
				((JTextField) c).setHorizontalAlignment(SwingConstants.RIGHT);
			}
		}
		buttonsPanel.setBackground(LAKER_BLUE);
		buttonsPanel.setBorder(ADD_SMALL_MARGIN);
		for (Component c: buttonsPanel.getComponents()) {
			c.setFont(NORMAL_FONT);
		}
	}
	
	/****************************************************************
	 * Gets the text associated with buttons to determine the players.
	 * @param buttonGroupOne the button group for the radio buttons
	 * @param buttonGroupTwo the button group for the radio buttons
	 * @return an ArrayList of the text associated with the buttons
	 ****************************************************************/
	public ArrayList<String> getSelectedButtonText(
			final ButtonGroup buttonGroupOne, 
			final ButtonGroup buttonGroupTwo) {
		ArrayList<String> selectedText = new ArrayList<String>();
        for (Enumeration<AbstractButton> buttons = buttonGroupOne.getElements();
        		buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                selectedText.add(button.getText());
            }
        }
        
        for (Enumeration<AbstractButton> buttons = buttonGroupTwo.getElements();
        		buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                selectedText.add(button.getText());
            }
        }
        return selectedText;
    }
	
	/*********************************************
	 * Checks if a player's name has numbers in it.
	 * @param name is the name provided by the user
	 * @return a boolean whether there are numbers
	 **********************************************/
	private boolean nameCheck(final String name) {
		if (name.matches(".*\\d+.*")) {
			return true;
		}
		return false;
	}
	
	/**************************************************************
	 * Process the text from the buttons that were clicked.
	 * @return An array of parameters for the game
	 **************************************************************/
	 public QuartoLogic processButtonText() {
		 String playerOneName = "";
		 String playerTwoName = "";
		 QuartoLogic quarto;
		 ArrayList<String> selectedButtonText = getSelectedButtonText(
				 playerOneGroup, playerTwoGroup);
		 ArrayList<String> selectedAIButtonText = getSelectedButtonText(
				 playerOneAIGroup, playerTwoAIGroup);
		 String firstSelectedButton = selectedButtonText.get(0);
		 String secondSelectedButton = selectedButtonText.get(1);
		 String aiName = "AI";
		 switch(firstSelectedButton) {
		 case "Human":
			 playerOneName = playerOneText.getText();
			 if (playerOneName.isEmpty() || nameCheck(playerOneName)) {
				 while (playerOneName.isEmpty() || nameCheck(playerOneName)) {
					 playerOneName = JOptionPane.showInputDialog(
								"Please enter a valid name for player 1");
				 }
			 }
			 if (secondSelectedButton.equals("Human")) {
				 playerTwoName = playerTwoText.getText();
				 if (playerTwoName.isEmpty() || nameCheck(playerTwoName)) {
					 while (playerTwoName.isEmpty() 
							 || nameCheck(playerTwoName)) {
						 playerTwoName = JOptionPane.showInputDialog(
									"Please enter a valid name for player 2");
					 }
				 }
				 Player p1 = new Player(playerOneName);
				 Player p2 = new Player(playerTwoName);
				 quarto = new QuartoLogic(p1, p2);
				 return quarto;
				 
			 }  else { 
				 // Player two is AI so we have to check the difficulty 
				 // selected
				 Player p1 = new Player(playerOneName);
				 
				 String difficulty = selectedAIButtonText.get(1);
				 int diffLevel = Integer.parseInt(difficulty);
				 Player p2 = new Player(aiName, true, diffLevel);
				 quarto = new QuartoLogic(p1, p2);
				 return quarto;
				 
			 }
		 case "AI":
			 if (secondSelectedButton.equals("Human")) {
				 playerTwoName = playerTwoText.getText();
				 if (playerTwoName.isEmpty() || nameCheck(playerTwoName)) {
					 while (playerTwoName.isEmpty() 
							 || nameCheck(playerTwoName)) {
						 playerTwoName = JOptionPane.showInputDialog(
									"Please enter a valid name for player 2");
					 }
				 }
				 String difficulty = selectedAIButtonText.get(0);
				 int diffLevel = Integer.parseInt(difficulty);
				 Player p1 = new Player(aiName, true, diffLevel);
				 Player p2 = new Player(playerTwoName);
				 quarto = new QuartoLogic(p1, p2);
				 return quarto;
			 } else {
				 String difficultyOne = selectedAIButtonText.get(0);
				 String difficultyTwo = selectedAIButtonText.get(1);
				 int diffLevelOne = Integer.parseInt(difficultyOne);
				 int diffLevelTwo = Integer.parseInt(difficultyTwo);
				 Player p1 = new Player(aiName, true, diffLevelOne);
				 Player p2 = new Player(aiName, true, diffLevelTwo);
				 quarto = new QuartoLogic(p1, p2);
				 return quarto;
			 }
		default:
			playerOneName = playerOneText.getText();
			 if (playerOneName.isEmpty() || nameCheck(playerOneName)) {
				 while (playerOneName.isEmpty() || nameCheck(playerOneName)) {
					 playerOneName = JOptionPane.showInputDialog(
								"Please enter a valid name for player 1");
				 }
			 }
			 playerTwoName = playerTwoText.getText();
			 if (playerTwoName.isEmpty() || nameCheck(playerTwoName)) {
				 while (playerTwoName.isEmpty() || nameCheck(playerTwoName)) {
					 playerTwoName = JOptionPane.showInputDialog(
								"Please enter a valid name for player 2");
				 }
			 }
			 Player p1 = new Player(playerOneName);
			 Player p2 = new Player(playerTwoName);
			 quarto = new QuartoLogic(p1, p2);
			 return quarto;
			 
		 }
	 }
	 
		/**********************************************************
		 * Listens for events on the board and updates it as needed.
		 * @param e the action that was triggered by the user
		 **********************************************************/
	 	@Override
		public void actionPerformed(final ActionEvent e) {
			JButton click = (JButton) e.getSource();
			
			if (click.equals(ok)) {
				QuartoLogic quarto = (QuartoLogic) processButtonText();
				GameGUI mainPanel = new GameGUI(parent, quarto);
				parent.remove(this);
				parent.add(mainPanel);
				parent.repaint();
				parent.revalidate();
			}
			if (click.equals(cancel)) {
				System.exit(0);
			}
			if (click.equals(about)) {
				JOptionPane.showMessageDialog(this,
						"Quarto is a multi player strategic board game " 
						+ "featuring options\n to play versus another human "
						+ "player, computer AI or AI versus AI\n Developers: " 
						+ "Frank Wanye, Kyle Hekhuis, Gloire Rubambiza\n" 
						+ "Release 2 : December 2016", "About Quarto", 
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
}
