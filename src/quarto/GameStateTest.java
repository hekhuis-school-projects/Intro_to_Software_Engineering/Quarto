package quarto;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

/***************************
 * Test the GameState class.
 * @author Kyle Hekhuis
 * @since 10/20/16
 **************************/
public final class GameStateTest {

	/************************
	 * Test the constructor.
	 ***********************/
	@Test
	public void constructorTest() {
		Board testBoard = new Board(4,4);
		Player testPlayer1 = new Player("p1", true, 6);
		Player testPlayer2 = new Player("p2");
		int[][] testCounterRows = new int[4][4];
		int[][] testCounterColumns = new int[4][4];
		int[][] testCounterDiagonals = new int [4][4];
		ArrayList<Piece> testAvailablePieces = new ArrayList<Piece>();
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		testAvailablePieces.add(testPiece);
		int[] testScore = new int[] {0, 0};
		GameState gsTest = new GameState(testBoard,
										 testPlayer1,
										 testPlayer2,
										 testPlayer2, 
										 testCounterRows, 
										 testCounterColumns, 
										 testCounterDiagonals,
										 testAvailablePieces,
										 testScore,
										 testPiece);
		assertEquals(gsTest.getBoard(), testBoard);
		assertEquals(gsTest.getPlayer1(), testPlayer1);
		assertEquals(gsTest.getPlayer2(), testPlayer2);
		assertEquals(gsTest.getCurrentPlayer(), testPlayer2);
		int[][] rows = gsTest.getCounterRows();
		for (int i = 0; i < rows.length; i++) {
			for (int j = 0; j < rows[i].length; j++) {
				assertEquals(rows[i][j], testCounterRows[i][j]);
			}
		}
		int[][] cols = gsTest.getCounterColumns();
		for (int i = 0; i < cols.length; i++) {
			for (int j = 0; j < cols[i].length; j++) {
				assertEquals(cols[i][j], testCounterColumns[i][j]);
			}
		}
		int[][] diag = gsTest.getCounterDiagonals();
		for (int i = 0; i < diag.length; i++) {
			for (int j = 0; j < diag[i].length; j++) {
				assertEquals(diag[i][j], testCounterDiagonals[i][j]);
			}
		}
		assertEquals(gsTest.getAvailablePieces(), testAvailablePieces);
		assertEquals(gsTest.getScore()[0], testScore[0]);
		assertEquals(gsTest.getScore()[1], testScore[1]);
		assertEquals(gsTest.getSelectedPiece(), testPiece);
	}
	
//	@Test
//	public void saveAndLoadTest() {
//		Board testBoard = new Board(4,4);
//		Player testPlayer1 = new Player("p1", true, 6);
//		Player testPlayer2 = new Player("p2");
//		int[][] testCounterRows = new int[4][4];
//		int[][] testCounterColumns = new int[4][4];
//		int[][] testCounterDiagonals = new int [2][4];
//		ArrayList<Piece> testAvailablePieces = new ArrayList<Piece>();
//		Piece testPiece = new Piece(
//				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG, null);
//		testAvailablePieces.add(testPiece);
//		int[] testScore = new int[] {0, 0};
//		GameState gsTest1 = new GameState(testBoard,
//										 testPlayer1,
//										 testPlayer2,
//										 testPlayer2, 
//										 testCounterRows, 
//										 testCounterColumns, 
//										 testCounterDiagonals,
//										 testAvailablePieces,
//										 testScore,
//										 testPiece);
//		gsTest1.save();
//		GameState gsTest2 = GameState.load();
//		assertEquals(gsTest2.getBoard(), testBoard);
//		assertEquals(gsTest2.getPlayer1(), testPlayer1);
//		assertEquals(gsTest2.getPlayer2(), testPlayer2);
//		assertEquals(gsTest2.getCurrentPlayer(), testPlayer2);
//		int[][] rows = gsTest2.getCounterRows();
//		for (int i = 0; i < rows.length; i++) {
//			for (int j = 0; j < rows[i].length; j++) {
//				assertEquals(rows[i][j], testCounterRows[i][j]);
//			}
//		}
//		int[][] cols = gsTest2.getCounterColumns();
//		for (int i = 0; i < cols.length; i++) {
//			for (int j = 0; j < cols[i].length; j++) {
//				assertEquals(cols[i][j], testCounterColumns[i][j]);
//			}
//		}
//		int[][] diag = gsTest2.getCounterDiagonals();
//		for (int i = 0; i < diag.length; i++) {
//			for (int j = 0; j < diag[i].length; j++) {
//				assertEquals(diag[i][j], testCounterDiagonals[i][j]);
//			}
//		}
//		assertEquals(gsTest2.getAvailablePieces(), testAvailablePieces);
//		assertEquals(gsTest2.getScore()[0], testScore[0]);
//		assertEquals(gsTest2.getScore()[1], testScore[1]);
//		assertEquals(gsTest2.getSelectedPiece(), testPiece);
//	}
}
