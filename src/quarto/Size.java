package quarto;

/*************************************************************************
 * The Size enum determines the size of a quarto board piece.
 * @author Frank
 * @since 09/17/2016
 ************************************************************************/
public enum Size {
	
	/** Small piece. */
	SMALL, 
	
	/** Big piece. */
	BIG
	
}
