package quarto;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/************************
 * Test the Board class.
 * @author Kyle Hekhuis
 * @since 10/20/16
 ***********************/
public final class BoardTest {

	/***********************************************************
	 * Test constructor Board(int rows, int columns) along with
	 * getEmptyCells().
	 **********************************************************/
	@Test
	public void constructorTest() {
		Board testBoard = new Board(4,4);
		ArrayList<List<Integer>> emptyCells = testBoard.getEmptyCells();
		assertEquals(emptyCells.get(0).size(), 16);
		assertEquals(emptyCells.get(1).size(), 16);
		assertTrue(testBoard.isEmpty());
	}
	
	/****************************************************************
	 * Test placePiece(Piece piece, int row, int column) by placing
	 * piece into empty spot.
	 ***************************************************************/
	@Test
	public void placePieceTest1() {
		Board testBoard = new Board(4,4);
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		assertEquals(testBoard.placePiece(testPiece, 0, 0), true);
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
		assertFalse(testBoard.isEmpty());
		assertFalse(testBoard.isFull());
	}
	
	/***************************************************************
	 * Test placePiece(Piece piece, int row, int column) by placing
	 * piece into occupied spot.
	 **************************************************************/
	@Test
	public void placePieceTest2() {
		Board testBoard = new Board(4,4);
		Piece testPiece1 = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		Piece testPiece2 = new Piece(
				Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.BIG);
		assertEquals(testBoard.placePiece(testPiece1, 0, 0), true);
		assertEquals(testBoard.placePiece(testPiece2, 0, 0), false);
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
	}
	
	/****************************************
	 * Test removePiece(int row, int column).
	 ***************************************/
	@Test
	public void removePieceTest() {
		Board testBoard = new Board(4,4);
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		testBoard.placePiece(testPiece, 0, 0);
		testBoard.removePiece(0, 0);
		assertEquals(testBoard.getEmptyCells().get(0).size(), 16);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 16);
	}
	
	/********************
	 * Test clearBoard().
	 *******************/
	@Test
	public void clearBoardTest() {
		Board testBoard = new Board(4,4);
		Piece testPiece1 = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		Piece testPiece2 = new Piece(
				Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.BIG);
		testBoard.placePiece(testPiece1, 0, 0);
		testBoard.placePiece(testPiece2, 0, 1);
		testBoard.clearBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 16);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 16);
	}
	
	@Test
	public void testFullBoard() {
		Board testBoard = new Board(4, 4);
		Piece testPiece1 = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				testBoard.placePiece(testPiece1, i, j);
			}
		}
		assertTrue(testBoard.isFull());
	}
	
}
