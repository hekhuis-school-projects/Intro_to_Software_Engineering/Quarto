package quarto;

import java.util.Stack;

/********************************************************************
 * The MaxSizeStack class represents a stack that has a maximum size
 * it can possibly be. Once you begin to add things to the stack when
 * it's already at maximum capacity, it will keep removing from the
 * bottom of the stack until the stack is the proper size.
 * @author Kyle Hekhuis
 * @since 10/19/16
 * 
 * @param <E> the type of elements in this collection
 *******************************************************************/
public final class MaxSizeStack<E> extends Stack<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Max size the stack can be. */
	private int maxSize;
	
	/*********************************************
	 * Creates a stack object that has a max size.
	 * @param pMaxSize max size the stack can be
	 ********************************************/
	public MaxSizeStack(final int pMaxSize) {
		super();
		this.maxSize = pMaxSize;
	}
	
	@Override
	public E push(final E item) {
		while (this.size() >= maxSize) {
			this.remove(0);
		}
		return super.push(item);
	}
}
