package quarto;

import java.util.ArrayList;
import java.io.Serializable;

/***********************************************************
 * The GameState class represents a specific game state of Quarto.
 * It saves the current Quarto game's board, current player,
 * counters, available pieces, score. and selected piece.
 * @author Kyle Hekhuis
 * @since 10/19/16
 **********************************************************/
public final class GameState implements Serializable {

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** State of the board in Quarto game. */
	private Board board;
	
	/** State of player 2 in Quarto game.*/
	private Player player1;
	
	/** State of player 2 in Quarto game.*/
	private Player player2;
	
	/** State of the current player in Quarto game.*/
	private Player currentPlayer;
	
	/** State of the row counters in Quarto game. */
	private int[][] counterRows;
	
	/** State of the column counters in Quarto game. */
	private int[][] counterColumns;
	
	/** State of the diagonal counters in Quarto game. */
	private int[][] counterDiagonals;
	
	/** State of the available pieces in Quarto game. */
	private ArrayList<Piece> availablePieces;
	
	/** State of the score in Quarto game. */
	private int[] score;
	
	/** State of the selectedPiece in Quarto game. */
	private Piece selectedPiece;
	
	/***************************************************
	 * Instantiates a state of the Quarto game with its
	 * current attributes.
	 * @param pBoard Quarto game board
	 * @param pPlayer1 player 1 of Quarto game
	 * @param pPlayer2 player 2 of Quarto game
	 * @param pCurrentPlayer Quarto current player
	 * @param pCounterRows Quarto row counters
	 * @param pCounterColumns Quarto column counters
	 * @param pCounterDiagonals Quarto diagonal counters
	 * @param pAvailablePieces Quarto available pieces
	 * @param pScore Quarto score
	 * @param pSelectedPiece Quarto selected piece
	 **************************************************/
	public GameState(final Board pBoard,
					 final Player pPlayer1,
					 final Player pPlayer2,
					 final Player pCurrentPlayer, 
					 final int[][] pCounterRows, 
					 final int[][] pCounterColumns, 
					 final int[][] pCounterDiagonals, 
					 final ArrayList<Piece> pAvailablePieces,
					 final int[] pScore,
					 final Piece pSelectedPiece) {
		this.board = pBoard;
		this.player1 = pPlayer1;
		this.player2 = pPlayer2;
		this.currentPlayer = pCurrentPlayer;
		this.counterRows = pCounterRows;
		this.counterColumns = pCounterColumns;
		this.counterDiagonals = pCounterDiagonals;
		this.availablePieces = pAvailablePieces;
		this.score = pScore;
		this.selectedPiece = pSelectedPiece;
	}
	
	/**********************************
	 * Returns this game state's board.
	 * @return this game state's board
	 *********************************/
	public Board getBoard() {
		return board;
	}
	
	/*******************************************
	 * Returns this game state's player 1.
	 * @return this game state's player 1
	 ******************************************/
	public Player getPlayer1() {
		return player1;
	}
	
	/*******************************************
	 * Returns this game state's player 2.
	 * @return this game state's player 2
	 ******************************************/
	public Player getPlayer2() {
		return player2;
	}
	
	/*******************************************
	 * Returns this game state's current player.
	 * @return this game state's current player
	 ******************************************/
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	/*****************************************
	 * Returns this game state's row counters.
	 * @return this game state's row counters
	 ****************************************/
	public int[][] getCounterRows() {
		int[][] temp = this.counterRows;
		return temp;
	}
	
	/********************************************
	 * Returns this game state's column counters.
	 * @return this game state's column counters
	 *******************************************/
	public int[][] getCounterColumns() {
		int[][] temp = this.counterColumns;
		return temp;
	}
	
	/**********************************************
	 * Returns this game state's diagonal counters.
	 * @return this game state's diagonal counters
	 *********************************************/
	public int[][] getCounterDiagonals() {
		int[][] temp = this.counterDiagonals;
		return temp;
	}
	
	/*********************************************
	 * Returns this game state's available pieces.
	 * @return this game state's available pieces
	 ********************************************/
	public ArrayList<Piece> getAvailablePieces() {
		return availablePieces;
	}
	
	/**********************************
	 * Returns this game state's score.
	 * @return this game state's score
	 *********************************/
	public int[] getScore() {
		int[] temp = this.score;
		return temp;
	}
	
	/*******************************************
	 * Returns this game state's selected piece.
	 * @return this game state's selected piece.
	 *******************************************/
	public Piece getSelectedPiece() {
		return selectedPiece;
	}
}