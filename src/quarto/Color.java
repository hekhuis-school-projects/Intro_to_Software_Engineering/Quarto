package quarto;

/*************************************************************************
 * The Color enum determines the color of a quarto board piece.
 * @author Frank
 * @since 09/17/2016
 ************************************************************************/
public enum Color {
	
	/** Black color. */
	BLACK,
	
	/** Blue color. */
	BLUE
}
