package quarto;

/**************************************************************************
 * The Hollow enum determines whether a quarto game piece is hollow or not.
 * @author Frank
 * @since 09/17/2016
 **************************************************************************/
public enum Hollow {
	
	/** Hollow piece. */
	HOLLOW, 
	
	/** Solid piece. */
	SOLID
	
}
