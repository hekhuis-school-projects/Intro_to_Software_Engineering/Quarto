package quarto;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

/*************************************************************
 * The Board class represents a game board which is a 2D
 * array of cells. Has the ability to place and remove pieces
 * and clear the board.
 * @author Kyle Hekhuis
 * @since 10/03/16
 ************************************************************/
public final class Board implements Serializable {

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** 2D array of cells making up the board.*/
	private Cell[][] board;
	
	/** Amount of rows in the board. */
	private int rows;
	
	/** Amount of columns in the board. */
	private int columns;
	
	/*************************************************
	 * Instantiates a Board whose size is given by
	 * the row and column sizes passed to it.
	 * @param pRows amount of rows the board has
	 * @param pColumns amount of columns the board has
	 ************************************************/
	public Board(final int pRows, final int pColumns) {
		this.rows = pRows;
		this.columns = pColumns;
		board = buildBoard(this.rows, this.columns);
	}
	
	/*************************************************************
	 * Build a new Board object from this Board.
	 * @param other is the Board from which this one is being built.
	 **************************************************************/
	public Board(final Board other) {
		this.rows = other.rows;
		this.columns = other.columns;
		this.board = new Cell[rows][columns];
		for (int row = 0; row < rows; ++row) {
			for (int col = 0; col < columns; ++col) {
				Cell temp = other.board[row][col];
				board[row][col] = new Cell(temp.isOccupied(), 
						temp.getPiece());   
			}
		}
	}
	
	/********************************************************
	 * Builds the game board by instantiating all the cells.
	 * @param pRows amount of rows the board has
	 * @param pColumns amount of columns the board has
	 * @return Cell[][] with all the instantiated cells
	 ********************************************************/
	private Cell[][] buildBoard(final int pRows, final int pColumns) {
		Cell[][] tempBoard = new Cell[pRows][pColumns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				tempBoard[i][j] = new Cell();
			}
		}
		return tempBoard;
	}
	
	/**************************************************
	 * Places a piece on a certain spot on the board.
	 * @param piece piece to be placed on board
	 * @param row row to place the piece into
	 * @param column column to place the piece into
	 * @return true if piece was placed successfully
	 *************************************************/
	public boolean placePiece(final Piece piece, 
							  final int row, final int column) {
		if (board[row][column].isOccupied() || piece == null) {
			return false;
		} else {
			board[row][column].setOccupied(true);
			board[row][column].setPiece(piece);
			return true;
		}
	}
	
	/***********************************************************
	 * Removes the piece in the cell located Cell[row][column].
	 * @param row row the piece is located in
	 * @param column column the piece is located in
	 **********************************************************/
	public void removePiece(final int row, final int column) {
		board[row][column].setOccupied(false);
		board[row][column].setPiece(null);
	}
	
	/*********************************************************
	 * Clears the board by using the removePiece() method on
	 * all the cells in the board.
	 ********************************************************/
	public void clearBoard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				removePiece(i, j);
			}
		}
	}
	
	/********************************************************************
	 * Loops through board array, and constructs an ArrayList of the row
	 * and column numbers of the empty cells in the board.
	 * @return an ArrayList<List<Integer>> object, containing the rows
	 * and corresponding columns of empty cells in the game board.
	 * list.get(0) contains the row numbers, list.get(1) contains the
	 * column numbers
	 * @author Frank 
	 *******************************************************************/
	public ArrayList<List<Integer>> getEmptyCells() {
		
		ArrayList<List<Integer>> emptyCells = new ArrayList<List<Integer>>();
		List<Integer> tempRows = new ArrayList<Integer>();
		List<Integer> tempCols = new ArrayList<Integer>();
		for (int i = 0; i < board.length; ++i) {
			for (int j = 0; j < board[i].length; ++j) {
				if (!board[i][j].isOccupied()) {
					tempRows.add(i);
					tempCols.add(j);
				}
			}
		}
		emptyCells.add(tempRows);
		emptyCells.add(tempCols);
		
		return emptyCells;
	}
	
	/*******************************************************************
	 * Returns this board object.
	 * @return the 2-D array of cells representing the current state of
	 * the board.
	 * ****************************************************************/
	public Cell[][] getBoard() {
		Cell[][] temp = this.board;
		return temp;
	}
	
	/****************************
	 * Clones the board.
	 * @return clone of the board
	 ***************************/
	public Board cloneBoard() {
		Board b = new Board(rows, columns);
		for (int i = 0; i < board.length; ++i) {
			for (int j = 0; j < board[i].length; ++j) {
				b.board[i][j].setOccupied(board[i][j].isOccupied());
				b.board[i][j].setPiece(board[i][j].getPiece());
			}
		}
		return b;
	}
	
	/*************************************
	 * Tells if the board is empty or not.
	 * @return true if the board is empty, false otherwise
	 ***********************************/
	public boolean isEmpty() {
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				if (board[i][j].isOccupied()) {
					return false;
				}
			}
		}
		return true;
	}
	
	/***************************************
	 * Tells if the board is full or not.
	 * @return true if the board is full, false otherwise
	 *********************************************/
	public boolean isFull() {
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				if (!board[i][j].isOccupied()) {
					return false;
				}
			}
		}
		return true;
	}
	
}
