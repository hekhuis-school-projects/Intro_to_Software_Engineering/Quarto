/**
 * 
 */
package quarto;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/************************************************************************
 * Tests relevant methods from the Player class
 * @author Frank
 * @date 10/13/2016
 ************************************************************************/
public class PlayerTest {
	
	private Player hPlayer, aiPlayer;
	
	private ArrayList<Piece> availablePieces;
	
	@Test
	public void playerConstructorTest() {
		Player testPlayer = new Player("Player");
		assertEquals(testPlayer.getName(), "Player");
		assertEquals(testPlayer.isAI(), false);
		Player testPlayer2 = new Player("Player2");
		assertEquals(testPlayer2.getName(), "Player2");
		assertEquals(testPlayer2.isAI(), false);
		assertNotEquals(testPlayer, testPlayer2);
	}
	
	@Test
	public void aiConstructorTest() {
		Player testPlayer = new Player("Player", true, 6);
		assertEquals(testPlayer.getName(), "Player");
		assertEquals(testPlayer.isAI(), true);
		Player testPlayer2 = new Player("Player2", false, 5);
		assertEquals(testPlayer2.getName(), "Player2");
		assertEquals(testPlayer2.isAI(), false);
		assertNotEquals(testPlayer, testPlayer2);
	}
	
	@Before
	public void createBaseStuff() {
		hPlayer = new Player("Player");
		aiPlayer = new Player("Player2", true, 3);

		availablePieces = new ArrayList<Piece>();
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.ROUND, Size.SMALL));
	}
	
	@Test
	public void equalsPlayerTest() {
		Player other = new Player("Player");
		assertTrue(hPlayer.equals(other));
		other = new Player("Player", true, 4);
		assertFalse(hPlayer.equals(other));
		other = new Player("Player2", true, 4);
		assertTrue(aiPlayer.equals(other));
		assertFalse(aiPlayer.equals(hPlayer));
	}
	
	@Test
	public void equalsObjectTest() {
		assertTrue(hPlayer.equals(new Player("Player", false, 0)));
		assertFalse(hPlayer.equals(new Player("Player2", false, 0)));
		assertTrue(aiPlayer.equals(new Player("Player2", true, 3)));
		assertFalse(aiPlayer.equals(new Player("Player", true, 6)));
	}
	
	@Test
	public void placePieceNullArgumentTest() {
		ArrayList<List<Integer>> cells = null;
		for(int i = 0; i < 100; ++i) {
			int[] cell = aiPlayer.placePiece(cells);
			assertEquals(cell[0], -1);
			assertEquals(cell[1], -1);
			assertEquals(cell.length, 2);
		}
	}
	
	@Test
	public void placePieceInvalidArgumentTest() {
		ArrayList<List<Integer>> cells = new ArrayList<List<Integer>>();
		List<Integer> rows = new ArrayList<Integer>();
		rows.add(1);
		rows.add(2);
		rows.add(3);
		List<Integer> cols = new ArrayList<Integer>();
		rows.add(1);
		cells.add(rows);
		cells.add(cols);
		for(int i = 0; i < 100; ++i) {
			int[] cell = aiPlayer.placePiece(cells);
			assertEquals(cell[0], -1);
			assertEquals(cell[1], -1);
			assertEquals(cell.length, 2);
		}
	}
	
	@Test
	public void placePieceEmptyArgumentTest() {
		ArrayList<List<Integer>> cells = new ArrayList<List<Integer>>();
		if(cells.isEmpty())
			System.out.println("Empty list");
		for(int i = 0; i < 100; ++i) {
			int[] cell = aiPlayer.placePiece(cells);
			assertEquals(cell[0], -1);
			assertEquals(cell[1], -1);
			assertEquals(cell.length, 2);
		}
	}
	
	@Test
	public void placePieceTest() {
		ArrayList<List<Integer>> cells = new ArrayList<List<Integer>>();
		List<Integer> rows = new ArrayList<Integer>();
		for(int i = 0; i < 16; ++i)
			rows.add(i);
		List<Integer> cols = new ArrayList<Integer>();
		for(int i = 0; i < 16; ++i)
			cols.add(i);
		cells.add(rows);
		cells.add(cols);
		for(int i = 0; i < 100; ++i) {
			int[] cell = aiPlayer.placePiece(cells);
			assertTrue(cell[0] > -1 && cell[0] < 16);
			assertTrue(cell[1] > -1 && cell[1] < 16);
			assertEquals(cell.length, 2);
		}
	}
	
	@Test
	public void selectPieceTest() {
		int size = availablePieces.size();
		while(size > 0) {
			int num = hPlayer.selectPiece(availablePieces);
			assertTrue(num >= 0 && num <= size);
			availablePieces.remove(num);
			size = availablePieces.size();
		}
	}
	
	@Test
	public void selectPieceNullArgumentTest() {
		int num = hPlayer.selectPiece(null);
		assertTrue(num == -1);
		availablePieces = null;
		num = hPlayer.selectPiece(availablePieces);
		assertTrue(num == -1);
	}
	
	@Test
	public void selectPieceEmptyArgumentTest() {
		availablePieces.clear();
		int num = hPlayer.selectPiece(availablePieces);
		assertTrue(num == -1);
	}

}
