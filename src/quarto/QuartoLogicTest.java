package quarto;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

/**
 * Test the QuartoLogic class.
 * @author Kyle Hekhuis
 * @since 10/20/16
 */
public final class QuartoLogicTest {

	/***************************************************************
	 * Test the placePiece(Piece piece, int row, int column) method.
	 **************************************************************/
	@Test
	public void placePieceTest1() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		assertEquals(testQuartoLogic.placePiece(testPiece, 0, 0), true);
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
	}
	
	/**************************************************
	 * Test the placePiece(int row, int column) method.
	 **************************************************/
	@Test
	public void placePieceTest2() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.selectPiece(0);
		assertEquals(testQuartoLogic.placePiece(0, 0), true);
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
	}
	
	/**
	 * Test the placePiece(Piece piece, int row, int column) method for
	 * placing in occupied spot.
	 */
	@Test
	public void placePieceTest3() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		Piece testPiece1 = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		Piece testPiece2 = new Piece(
				Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.BIG);
		assertEquals(testQuartoLogic.placePiece(testPiece1, 0, 0), true);
		assertEquals(testQuartoLogic.placePiece(testPiece2, 0, 0), false);
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
	}
	
	/******************************************************
	 * Test the placePiece(int row, int column) method for
	 * placing in occupied spot.
	 *****************************************************/
	@Test
	public void placePieceTest4() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.selectPiece(0);
		assertEquals(testQuartoLogic.placePiece(0, 0), true);
		testQuartoLogic.selectPiece(1);
		assertEquals(testQuartoLogic.placePiece(0, 0), false);
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 15);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 15);
	}
	
	/********************************
	 * Test the clearBoard() method.
	 ********************************/
	@Test
	public void clearBoardTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.selectPiece(0);
		testQuartoLogic.placePiece(0, 0);
		testQuartoLogic.selectPiece(1);
		testQuartoLogic.placePiece(1, 1);
		testQuartoLogic.clearBoard();
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 16);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 16);
	}
	
	/*******************************
	 * Test the undoMove() method.
	 ******************************/
	@Test
	public void undoTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.selectPiece(0);
		testQuartoLogic.placePiece(0, 0);
		testQuartoLogic.undoMove();
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 16);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 16);
	}
	
	/******************************
	 * Test the isWinner() method.
	 *****************************/
	@Test
	public void isWinnerTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		assertEquals(testQuartoLogic.isWinner(), false);
	}
	
	/****************************
	 * Test the isDraw() method.
	 ****************************/
	@Test
	public void isDrawTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		assertEquals(testQuartoLogic.isDraw(), false);
	}
	
	/******************************
	 * Test the nextTurn() method.
	 ******************************/
	@Test
	public void nextTurnTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.nextTurn();
		assertEquals(testQuartoLogic.getCurrentPlayer(), testPlayer2);
	}
	
	/*******************************
	 * Test the nextRound() method.
	 *******************************/
	@Test
	public void nextRoundTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.nextRound();
		Board testBoard = testQuartoLogic.getBoard();
		assertEquals(testBoard.getEmptyCells().get(0).size(), 16);
		assertEquals(testBoard.getEmptyCells().get(1).size(), 16);
		assertEquals(testQuartoLogic.isWinner(), false);
		assertEquals(testQuartoLogic.isDraw(), false);
	}
	
	/******************************
	 * Test the getScore() method.
	 ******************************/
	@Test
	public void getScoreTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		assertEquals(testQuartoLogic.getScore()[0], 0);
		assertEquals(testQuartoLogic.getScore()[1], 0);
	}
	
	/******************************
	 * Test the getPieces() method.
	 ******************************/
	@Test
	public void getPiecesTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		ArrayList<Piece> tempPieces = testQuartoLogic.getPieces();
		assertEquals(tempPieces.size(), 16);
	}
	
	/**************************************
	 * Test the getCurrentPlayer() method.
	 **************************************/
	@Test
	public void getCurrentPlayerTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		assertEquals(testQuartoLogic.getCurrentPlayer(), testPlayer1);
	}
	
	/********************************
	 * Test the getSelected() method.
	 ********************************/
	@Test
	public void getSelectedTest() {
		Player testPlayer1 = new Player("p1");
		Player testPlayer2 = new Player("p2");
		QuartoLogic testQuartoLogic = new QuartoLogic(testPlayer1, testPlayer2);
		testQuartoLogic.selectPiece(0);
		Piece testPiece = new Piece(Hollow.HOLLOW, Color.BLACK, 
									Shape.SQUARE, Size.BIG);
		assertEquals(testQuartoLogic.getSelected(), testPiece);
	}
}
