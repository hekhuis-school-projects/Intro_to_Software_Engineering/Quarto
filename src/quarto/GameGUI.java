package quarto;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.border.Border;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*****************************************************************************
* The class instantiates the board for the game.
* It handles any new events performed in the game.
* @author Gloire Rubambiza
* @since 12/06/2016
******************************************************************************/
public final class GameGUI extends JPanel implements ActionListener {
	
	
	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/**Gets the screen size. */
	private static final double WIDTH = 
			Toolkit.getDefaultToolkit().getScreenSize().getWidth();

	/** Creates a border constant. */
	private static final Border CREATE_EMPTY_BORDER =
			BorderFactory.createEmptyBorder(10, 10, 10, 10);

	/** Sets the board size. */ 
	private static final int BOARD_SIZE = 4;
	
	/** Stores other info for the board. */
	private final int pieceWidth, pieceHeight;

	/** Instantiates all the board buttons. */
	private JButton[][] bdButtons;

	/** Instantiates all different pieces of the game. */
	private JButton[][] pieceButtons;

	/** Menu items to handle settings of a game. */
	private JButton newGame, exit;

	/** Panel to contain the Menu and its menu items. */
	private JPanel menuPanel;

	/** Panel to contain all the buttons and game pieces. */
	private JPanel boardPanel;
	
	/** Panel to contain the menu panel, pieces and undo buttons. */
	private JPanel southPanel;
	
	/** Panel to contain undo redo buttons. */
	private JPanel undoPanel;
	
	/** Panel that contains the 'QUARTO' label and the next piece to display. */
	private JPanel headerPanel;
	
	/** Labels for players actions and score status and undo actions. */
	private JLabel playerOneName, playerTwoName, playerOneScore, playerTwoScore;
	
	/** Buttons for undo, save, and load actions. */
	private JButton undo, save, load;
	
	/** Panel to contain the buttons for the pieces. */
	private JPanel piecesPanel;
	
	/** Quarto object to be used to call the methods. */
	private QuartoLogic quarto;
	
	/** Next piece to be used for play by the opponent. */
	private JLabel nextPiece, nextPieceLabel;
	
	/** The Label with the QUARTO name. */
	private static final JLabel QUARTO = new JLabel("QUARTO");
	
	/** Holds the Icon of the selected piece. */
	private Icon selectedIcon;


	/** Panel that holds information about the players and their scores. */
	private JPanel playerPanel;

	/** Displays the name of player 1. */
	private JLabel playerOneNameLabel;

	/** Displays the score of player 1. */
	private JLabel playerOneScoreLabel;

	/** Displays the name of player 2. */
	private JLabel playerTwoNameLabel;

	/** Displays the score of player 2. */
	private JLabel playerTwoScoreLabel;
	
	/** Displays the name of the player whose turn it is. */
	private JLabel currentPlayerLabel;
	
	/** The parent frame of this panel. */
	private JFrame parent;
	
	/** The blue color used for most of the backgrounds in the game. */
	private static final java.awt.Color LAKER_BLUE = 
			new java.awt.Color(0, 101, 164);
	
	/** The font for most text in the GUI. */
	private static final Font NORMAL_FONT = 
			new Font("Cooper Black", Font.PLAIN, 20);
	
	/** Instantiates a button size. */
	private static final int BUTTON_SIZE = 20;
	
	/** Text field size. */
	private static final int TEXTFIELD_SIZE = 15;
	
	/**********************************************************************
	 * Constructor for the GUI and all its items. 
	 * @param pParent is the parent frame of this panel.
	 * @param pQuarto contains the game logic and state displayed on this panel.
	 *****************************************************************/
	public GameGUI(final JFrame pParent, final QuartoLogic pQuarto) {
		this.parent = pParent;
		this.quarto = pQuarto;

		pieceWidth = BOARD_SIZE - 2;
		pieceHeight = BOARD_SIZE * 2;
				
		pieceButtons = new JButton[BOARD_SIZE][BOARD_SIZE];

		newGame = new JButton("NEW GAME"); //Icon can be added if we want to
		newGame.addActionListener(this);

		exit = new JButton("EXIT");
		exit.addActionListener(this);
		
		menuPanel = new JPanel();
		
		menuPanel.setLayout(new GridLayout(BOARD_SIZE - 2,
				BOARD_SIZE - 2));
		
		//Add menu items
		menuPanel.add(newGame);
		menuPanel.add(exit);
		
		playerOneNameLabel = new JLabel("Player 1: ");
		playerOneName = new JLabel(quarto.getPlayer1().getName());
		playerOneScoreLabel = new JLabel("Player 1 Score: ");
		playerOneScore = new JLabel("0");
		
		playerTwoNameLabel = new JLabel("Player 2: ");
		playerTwoName = new JLabel(quarto.getPlayer2().getName());
		playerTwoScoreLabel = new JLabel("Player 2 Score: ");
		playerTwoScore = new JLabel("0");
		
		undo = new JButton("UNDO MOVE");
		save = new JButton("SAVE GAME");
		load = new JButton("LOAD PREVIOUS GAME");
		
		undoPanel = new JPanel();
		undoPanel.setLayout(new GridLayout(BOARD_SIZE - 1, 1));
		undoPanel.add(undo);
		undoPanel.add(save);
		undoPanel.add(load);
		
		undo.addActionListener(this);
		save.addActionListener(this);
		load.addActionListener(this);
		
		piecesPanel = new JPanel();
		piecesPanel.setLayout(new GridLayout(
				BOARD_SIZE - pieceWidth, BOARD_SIZE * pieceHeight));
		
		pieceButtons = new JButton[BOARD_SIZE - pieceWidth][BOARD_SIZE * 2];
		for (int k = 0; k < BOARD_SIZE / 2; k++) {
			for (int l = 0; l < BOARD_SIZE * 2; l++) {
				pieceButtons[k][l] = setButton((k * BOARD_SIZE * 2) + l);
				piecesPanel.add(pieceButtons[k][l]);
			}
		}
		piecesPanel.setBackground(LAKER_BLUE);
		
		southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		southPanel.add(menuPanel, BorderLayout.WEST);
		southPanel.add(undoPanel, BorderLayout.EAST);
		southPanel.add(piecesPanel, BorderLayout.CENTER);

		boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(BOARD_SIZE, BOARD_SIZE));
		boardPanel.setBackground(LAKER_BLUE);
		
		bdButtons = new JButton[BOARD_SIZE][BOARD_SIZE];
		for (int i = 0; i < BOARD_SIZE; i++) {
			for (int j = 0; j < BOARD_SIZE; j++) {
				bdButtons[i][j] = new JButton("");
				bdButtons[i][j].setSize(BUTTON_SIZE, BUTTON_SIZE);
				bdButtons[i][j].addActionListener(this);
				bdButtons[i][j].setBackground(java.awt.Color.WHITE);
				boardPanel.add(bdButtons[i][j]);
			}
		}
		
		setPlayerPanel();
		
		//Adds the next piece to the major panel
		selectedIcon = loadImage("placeholder");
		currentPlayerLabel = new JLabel("<html>Current player: <br>" 
				+ quarto.getCurrentPlayer().getName() 
				+ "<br><br> Select a piece for your opponent to play");
		currentPlayerLabel.setFont(NORMAL_FONT);
		//Adds padding to currentPlayerLabel
		currentPlayerLabel.setBorder(CREATE_EMPTY_BORDER);
		nextPieceLabel = new JLabel("Next Piece to play: ");
		nextPieceLabel.setFont(NORMAL_FONT);
		//Added padding to the nextPieceLabel 
		nextPieceLabel.setBorder(CREATE_EMPTY_BORDER);
		nextPiece = new JLabel(selectedIcon);
		headerPanel = new JPanel();
		headerPanel.setLayout(new BorderLayout());
		headerPanel.add(currentPlayerLabel, BorderLayout.NORTH);
		headerPanel.add(nextPieceLabel, BorderLayout.CENTER);
		headerPanel.add(nextPiece, BorderLayout.SOUTH);
		headerPanel.setBackground(LAKER_BLUE);
		headerPanel.setMaximumSize(new Dimension(
				(int) WIDTH / BOARD_SIZE,
				headerPanel.getHeight()));
		headerPanel.setPreferredSize(new Dimension(
				(int) WIDTH / BOARD_SIZE,
				headerPanel.getHeight()));
		
		setMajorPanel();
		
		moveIfAi();
		
	}
	
	/************************************************************* 
	 * Sets the major panel to hold every other panel on the board.
	 *************************************************************/
	private void setMajorPanel() {
		// Contains all the other panels
		setLayout(new BorderLayout());
		add(boardPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		add(playerPanel, BorderLayout.EAST);
		
		//Adding the QUARTO Label
		QUARTO.setFont(new Font("Cooper Black", Font.PLAIN, 
				TEXTFIELD_SIZE * BOARD_SIZE));
		QUARTO.setHorizontalAlignment(JLabel.CENTER);
		//Added padding to the QUARTO header label
		QUARTO.setBorder(CREATE_EMPTY_BORDER);
		QUARTO.setBackground(LAKER_BLUE);
		add(QUARTO, BorderLayout.NORTH);
		add(headerPanel, BorderLayout.WEST);
		setBackground(LAKER_BLUE);
		
		setMaximumSize(new Dimension(
				(int) WIDTH / BOARD_SIZE,
				getHeight()));
		setPreferredSize(new Dimension(
				(int) WIDTH / BOARD_SIZE,
				getHeight()));
		
		this.setVisible(true);
	}
	
	/*****************************************
	 * Sets the player panel fonts and labels.  
	 *****************************************/
	private void setPlayerPanel() {
		playerPanel = new JPanel();
		playerPanel.setBackground(LAKER_BLUE);
		playerPanel.setLayout(new BoxLayout(playerPanel, BoxLayout.Y_AXIS));
		playerPanel.add(playerOneNameLabel);
		playerPanel.add(playerOneName);
		playerPanel.add(playerOneScoreLabel);
		playerPanel.add(playerOneScore);
		playerPanel.add(playerTwoNameLabel);
		playerPanel.add(playerTwoName);
		playerPanel.add(playerTwoScoreLabel);
		playerPanel.add(playerTwoScore);
		Component[] components = playerPanel.getComponents();
		for (int i = 0; i < components.length; ++i) {
			if (components[i] instanceof JLabel) {
				components[i].setFont(NORMAL_FONT);
				//Added padding to each JLabel in he playerPanel
				((JComponent) components[i]).setBorder(
						CREATE_EMPTY_BORDER);
			}
		}
	}

	/*************************************************************************
	 * Creates a JButton with the appropriate ImageIcon and size.
	 * @param index is the index of the Piece corresponding to this button
	 * in Quarto's availablePieces
	 * @return a JButton object
	 *************************************************************************/
	private JButton setButton(final int index) {
		
		String summary = quarto.getPieces().get(index).summary();
		Icon icon = loadImage(summary);
		JButton button = new JButton(icon);
		button.setSize(BUTTON_SIZE, BUTTON_SIZE);
		button.addActionListener(this);
		button.setBackground(java.awt.Color.WHITE);
		return button;
		
	}
	
	/**********************************************
	 * Resets the game between two current players.
	 * Repaints the board and some of the labels
	 *********************************************/
	private void setNewRound() {
		quarto.nextRound();
		Cell[][] board = quarto.getBoard().getBoard();

		for (int i = 0; i < BOARD_SIZE; ++i) {
			for (int j = 0; j < BOARD_SIZE; ++j) {

				if (!(board[i][j].isOccupied())) {
					bdButtons[i][j].setIcon(null);
				} else {	
					bdButtons[i][j].setIcon(
							loadImage(board[i][j].getPiece().summary()));
				}

			}
		}
		
		// Re-enable all the icons on the board for the pieces
		for (int k = 0; k < BOARD_SIZE / 2; k++) {
			for (int l = 0; l < BOARD_SIZE * 2; l++) {
				pieceButtons[k][l].setEnabled(true);
			}
		}
		
		currentPlayerLabel.setText(
				"<html>Current player: <br>" 
				+ quarto.getCurrentPlayer().getName() 
				+ "<br><br> Select a piece for your opponent "
				+ "to play");
		
		parent.revalidate();
		parent.repaint();
	}
	
	/*************************************************************************
	 * Loads an image icon from a .png file located in ..\\..\\res\\.
	 * @param fileName is the name of the icon file
	 * @return an ImageIcon of object containing the icon specified
	 ************************************************************************/
	private ImageIcon loadImage(final String fileName) {
		
		ImageIcon image = null;
		String path = "res\\" + fileName + ".png";
		
		//Checking if file is present on the system
		File file = new File(path);
		if (file.exists()) {
			image = new ImageIcon(path);
			//System.out.println(file.getAbsolutePath() + " exists on system.");
		} else {
			path = "res/" + fileName + ".png";
			file = new File(path);
			if (file.exists()) {
				image = new ImageIcon(path);
			} else {
				System.err.println(file.getAbsolutePath() 
						+ " does not exist on the system.");
			}
		}
		return image;
		
	}
		
		/**********************************************************
		 * Listens for events on the board and updates it as needed.
		 * @param e the action that was triggered by the user
		 **********************************************************/
		public void actionPerformed(final ActionEvent e) {
			//Get the source of the event
			JButton click = (JButton) e.getSource();
			
			//Access to current play to display the winner's name
			String currentPlayer = quarto.getCurrentPlayer().getName();
			if (click.equals(newGame)) {
				StartGame restart = new StartGame(parent);
				parent.remove(this);
				parent.add(restart);
				parent.repaint();
				parent.revalidate();
			}
			if (click.equals(save)) {
				saveGame();
			}
			if (click.equals(load)) {
				loadGame();
				drawBoard();
			}
			if (click.equals(exit)) {
				System.exit(0);
			}
			if (click.equals(undo)
					&& !quarto.getCurrentPlayer().isAI()) {
				System.out.println("Trying to undo a move");
				if (quarto.undoMove()) {
					System.out.println("Move undone");
					drawBoard();
					moveIfAi();
				}
				
			}
			// Wait for a piece to be selected by the user
			for (int r = 0; r < pieceButtons.length; r++) {
				for (int c = 0; c < pieceButtons[r].length; c++) {
				 
					if (click.equals(pieceButtons[r][c]) 
							&& quarto.getSelected() == null 
							&& !quarto.getCurrentPlayer().isAI()) {
						int index = ((BOARD_SIZE * 2) * r) + c;
						quarto.selectPiece(index);
						quarto.saveGameState();
						pieceButtons[r][c].setEnabled(false);
						selectedIcon = pieceButtons[r][c].getIcon();
						nextPiece.setIcon(selectedIcon);
						revalidate();
						repaint();
						quarto.nextTurn();
						currentPlayerLabel.setText(
								"<html>Current player: <br>" 
								+ quarto.getCurrentPlayer().getName() 
								+ "<br><br> Place the selected piece on the "
								+ "board.");
						moveIfAi();
					}
				}
			}
			
			//When a piece is selected, place it on the board
			for (int r = 0; r < bdButtons.length; r++) {
				for (int c = 0; c < bdButtons[r].length; c++) {
					Cell cell = quarto.getBoard().getBoard()[r][c];
					if (click == bdButtons[r][c] 
							&& quarto.getSelected() != null
							&& !cell.isOccupied()
							&& !quarto.getCurrentPlayer().isAI()) {
						quarto.placePiece(r, c);
						bdButtons[r][c].setIcon(selectedIcon);
						bdButtons[r][c].setFocusable(false);
						nextPiece.setIcon(loadImage("placeholder"));
						if (quarto.isWinner()) {
							String winner = currentPlayer;
							JOptionPane.showMessageDialog(parent, winner 
									+ " has won!");
							playerOneScore.setText("" + quarto.getScore()[0]);
							playerTwoScore.setText("" + quarto.getScore()[1]);
							setNewRound();
							moveIfAi();
						}
						if (quarto.isDraw()) {
							JOptionPane.showMessageDialog(parent, 
									"We have a draw!");
							setNewRound();
							moveIfAi();
						}
						currentPlayerLabel.setText(
								"<html>Current player: <br>" 
								+ quarto.getCurrentPlayer().getName() 
								+ "<br><br> Select a piece for your opponent "
								+ "to play");
						
					}
				}
			}
			
		
		}
	
	/*************************************************************************
	 * Draws the game board according to the information provided in the 
	 * Quarto class. Called when switching to next round, and after undo,
	 * and after AI moves.
	 ************************************************************************/
	private void drawBoard() {
		
		// Set icons of the game board
		Cell[][] board = quarto.getBoard().getBoard();
		for (int row = 0; row < BOARD_SIZE; ++row) {
			for (int col = 0; col < BOARD_SIZE; ++col) {
				Cell cell = board[row][col];
				JButton button = bdButtons[row][col];
				if (cell.isOccupied()) {
					button.setIcon(loadImage(cell.getPiece().summary()));
				} else {
					button.setIcon(null);
				}
			}
		}
		
		// Set icons of the pieces buttons
		ArrayList<Piece> allPieces = quarto.getPieces();
		for (int i = 0; i < allPieces.size(); i++) {
			// Convert index to row and column
			int row = 0;
			int col = 0;
			if (i < BOARD_SIZE * 2) {
				col = i;	
			} else {
				row = 1;
				col = i - (BOARD_SIZE * 2);
			}
			
			if (allPieces.get(i) == null) {
				pieceButtons[row][col].setEnabled(false);
			} else {
				pieceButtons[row][col].setEnabled(true);
			}
		}
		
		// Update the game status labels
		if (quarto.getSelected() == null) {
			nextPiece.setIcon(loadImage("placeholder"));
		} else {
			selectedIcon = loadImage(quarto.getSelected().summary());
			nextPiece.setIcon(selectedIcon);
		}
		
		// Checks if there is a draw or a winner, prints win message and 
		// starts new round if needeed
		if (quarto.isWinner()) {
			String winner = quarto.getCurrentPlayer().getName();
			JOptionPane.showMessageDialog(parent, winner 
					+ " has won!");
			playerOneScore.setText("" + quarto.getScore()[0]);
			playerTwoScore.setText("" + quarto.getScore()[1]);
			setNewRound();
			moveIfAi();
		}
		if (quarto.isDraw()) {
			JOptionPane.showMessageDialog(parent, 
					"We have a draw!");
			setNewRound();
			moveIfAi();
		}
		
		quarto.nextTurn();
		currentPlayerLabel.setText(
				"<html>Current player: <br>" 
				+ quarto.getCurrentPlayer().getName() 
				+ "<br><br> Place the selected piece on the "
				+ "board.");
		

		
	}
	
	/*************************************************************************
	 * Makes a move if the current player is an AI. Re-draws the board after
	 * every AI move, and makes current thread sleep for 3 seconds before each
	 * move.
	 ************************************************************************/
	private void moveIfAi() {
		
		// New version of this method
		// Not displaying... dammit
		currentPlayerLabel.setText(
				"<html>Current player: <br>" 
				+ quarto.getCurrentPlayer().getName() 
				+ " is thinking...");
		revalidate();
		while (quarto.getCurrentPlayer().isAI() 
				&& !quarto.isWinner()
				&& !quarto.isDraw()) {
			quarto.moveAI();
			// Repaint board
			drawBoard();
		}

	}
	
	/*******************************************************
	 * Saves the current game into the SavedGameState file
	 * using serialization of the QuartoLogic class.
	 *******************************************************/
	private void saveGame() {
		try {
			FileOutputStream fos = 
					new FileOutputStream("src/quarto/SavedGameState");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(quarto);
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/*********************************************************
	 * Loads the serialized version of the QuartoLogic class
	 * in the SavedGameState file and makes the current
	 * instance of QuartoLogic become the loaded one.
	 *********************************************************/
	private void loadGame() {
		try {
			FileInputStream fis = 
					new FileInputStream("src/quarto/SavedGameState");
			ObjectInputStream ois = new ObjectInputStream(fis);
			this.quarto = (QuartoLogic) ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
