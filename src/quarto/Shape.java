package quarto;

/*************************************************************************
 * The Shape enum determines the shape of a quarto game piece.
 * @author Frank
 * @since 09/17/2016
 ************************************************************************/
public enum Shape {
	
	/** Square piece. */
	SQUARE, 
	
	/** Round piece. */
	ROUND
	
}

