package quarto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.Serializable;

/*************************************************************************
 * Controller class for the players, whether human or AI. 
 * @author Frank
 * @since 09/23/2016
 */
public final class Player implements Serializable {

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** The name of the player. */
	private String name;
	
	/** Is the player AI? It is false initially. */
	private boolean ai = false;
	
	/** The difficulty of the AI. */
	private int difficulty = 0;
	
	/*********************************************************************
	 * Constructs a human player object with a given name.
	 * @param pName is the player's display name
	 ********************************************************************/
	public Player(final String pName) {
		this.name = pName;
	}
	
	/*********************************************************************
	 * Constructs a player object that is either human or AI with a given
	 * name.
	 * @param pName is the player's display name
	 * @param pAi is true when the player is an AI, false when it is a 
	 * human
	 * @param pDifficulty is the difficulty of this AI
	 ********************************************************************/
	public Player(final String pName, 
				  final boolean pAi, 
				  final int pDifficulty) {
		this.name = pName;
		this.ai = pAi;
		this.difficulty = pDifficulty;
	}
	
	/*********************************************************************
	 * Selects a row and column to place a Piece into.
	 * @param emptyCells is an ArrayList<Cell> object containing all the
	 * empty cells on the board
	 * @return {row, column} if the row and column were selected 
	 * successfully, {-1, -1} otherwise
	 ********************************************************************/
	public int[] placePiece(final ArrayList<List<Integer>> emptyCells) {
		
		if (emptyCells == null) {
			System.err.println("Received a null list of empty cells.");
			return new int[]{-1, -1};
		}
		if (emptyCells.isEmpty()) {
			System.err.println("No empty cells remaining on game board.");
			return new int[]{-1, -1};
		}
		if (emptyCells.get(0).size() != emptyCells.get(1).size()) {
			System.err.println("Received invalid empty cell input: the number"
					+ "of rows != number of columns provided.");
			return new int[]{-1, -1};
		}
		Random rand = new Random();
		//Returns random number between 0 and number of empty cells
		int randomNum = rand.nextInt(emptyCells.get(0).size());
		//In emptyCells, row 0 represents rows, row 1 represents columns
		int row = emptyCells.get(0).get(randomNum);
		int col = emptyCells.get(1).get(randomNum);
		return new int[]{row, col};
		
	}
	
	/*********************************************************************
	 * AI selects a random piece from the unplayed pieces in the game.
	 * @param pieces is an ArrayList<Piece> containing Quarto game pieces
	 * that have not been played yet
	 * @return the index of the Piece that has been selected by the AI
	 ********************************************************************/
	public int selectPiece(final ArrayList<Piece> pieces) {
		
		if (pieces == null) {
			System.err.println("Received a null list of empty cells.");
			return -1;
		}
		if (pieces.size() == 0) {
			System.err.println("No pieces remaining.");
			return -1;
		}
		Random rand = new Random();
		int randomNum = rand.nextInt(pieces.size());
		return randomNum;
		
	}

	/**
	 * Retrieves the player's chosen display name.
	 * @return name is the player's display name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the player's display name to a new value.
	 * @param pName is the player's display name
	 */
	public void setName(final String pName) {
		this.name = pName;
	}

	/**
	 * Checks whether the player is an AI player or not.
	 * @return true if the player is AI, false if the player is human
	 */
	public boolean isAI() {
		return ai;
	}

	/**
	 * Not intended to be used.
	 * Changes whether a player is controlled by a human or by the AI
	 * @param pAi is true if making the player AI controlled, false if making
	 * the player human controlled
	 */
	public void setAI(final boolean pAi) {
		this.ai = pAi;
	}
	
	/*******************************************************************
	 * Compares this Player object to another object.
	 * @param other is the other object to be compared.
	 * @return true, if the other object is an instance of Player AND 
	 * is equal to this Player object, false otherwise.
	 ******************************************************************/
	public boolean equals(final Object other) {
		if (other instanceof Player) {
			return (((Player) other).ai == this.ai 
					&& ((Player) other).name.trim().equals(this.name.trim()));
		} else {
			return false;
		}
	}
	
	/*******************************************************************
	 * Compares this Player object to another Player object.
	 * @param other is the other Player object to be compared.
	 * @return true, if the other Player object is equal to this 
	 * Player object, false otherwise.
	 ******************************************************************/
	public boolean equals(final Player other) {
		return (other.ai == this.ai && other.name.equals(this.name));
	}
	
	/*************************
	 * Clones this player.
	 * @return clone of player
	 *************************/
	public Player clonePlayer() {
		Player clone = new Player(name);
		clone.setAI(ai);
		return clone;
	}
	
	/******************************************************
	 * Returns a String representation of a Player object.
	 * @return a String representation of a Player object
	 ******************************************************/
	@Override
	public String toString() {
		return "Player Name: " + name + ", AI: " + ai;
	}
	
	/**************************************************************************
	 * Return's the AI's difficulty.
	 * @return an integer between 1 and 6. Returns 0 if the player is human.
	 *************************************************************************/
	public int getDifficulty() {
		if (ai) {
			return difficulty;
		} else {
			return 0;
		}
	}
	
}
