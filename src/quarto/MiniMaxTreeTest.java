package quarto;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MiniMaxTreeTest {

	private QuartoLogic quarto = null;
	
	private Player p1 = null;
	
	private Player p2 = null;
	
	private MiniMaxTree mmt;
	
	@Before
	public void setUpBeforeClass() {
		p1 = new Player("P1");
		p2 = new Player("P2");
		quarto = new QuartoLogic(p1, p2);
		mmt = new MiniMaxTree(quarto, 6);
	}

	@Test
	public void testBuildTree() {
		
		int[] answer = mmt.buildTree();
		assertTrue(answer.length == 3);
		for (int i: answer) {
			assertFalse(i == -1);
		}

		assertTrue(answer.length == 3);

		mmt = new MiniMaxTree(quarto, 0);
		answer = mmt.buildTree();
		for (int i: answer) {
			assertTrue(i == -1);
		}
		
		mmt = new MiniMaxTree(quarto, 6);
		quarto.selectPiece(0);
		for (int i = 0; i < 15; ++i) {
			if (i < 4)
				quarto.placePiece(0, i);
			else if (i < 8)
				quarto.placePiece(1, i - 4);
			else if (i < 12)
				quarto.placePiece(2, i - 8);
			else 
				quarto.placePiece(3, i - 12);
			quarto.selectPiece(i + 1);
		}
		quarto.placePiece(3, 3);
		
		answer = mmt.buildTree();
		for (int i: answer) {
			assertTrue(i == -1);
		}
		
	}

}
