package quarto;

import static org.junit.Assert.*;
import org.junit.Test;

/**************************
 * Test MaxSizeStack class.
 * @author Kyle Hekhuis
 * @since 10/20/16
 *************************/
public final class MaxSizeStackTest {

	/********************************************************************
	 * Test overriden push method to see if it's only allowing 5 entries
	 * and removing oldest entries as it fills past 5.
	 *******************************************************************/
	@Test
	public void pushTest() {
		MaxSizeStack<String> testStack = new MaxSizeStack<String>(5);
		testStack.push("1");
		testStack.push("2");
		testStack.push("3");
		testStack.push("4");
		testStack.push("5");
		testStack.push("6");
		testStack.push("7");
		assertEquals(testStack.get(0), "3");
	}
}
