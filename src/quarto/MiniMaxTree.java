package quarto;

import java.util.ArrayList;
import java.util.List;

/**
 * The MiniMax tree used by the AI to make moves. 
 * Can not look at all possible moves from start to end because that would
 * be 16! * 16! moves (about 3.4 * 10 ^ 26) possibilities. This is not 
 * feasible, so we have to look at only a certain number of moves - the 
 * number of moves we look at will be the "difficulty" of the AI.
 * @author Frank Derry Wanye
 * @since 12/02/2016
 */
public final class MiniMaxTree {
	
	/** The difficulty of this AI - looks at this number of possible moves. */
	private int difficulty;
	
	/** Smallest possible score. */
	private static final int NEGINF = -9999;
	
	/** Smallest possible score. */
	private static final int POSINF = 9999;
	
	/** Numbers. */
	private static final int FOURTEEN = 14, TWELVE = 12, FOUR = 4, THREE = 3;
	
	/** The head of this MiniMaxTree. */
	private QuartoLogic head = null;

	/**************************************************************************
	 * Constructs a MinMax Tree based on the current state of the game, and 
	 * the difficulty of the AI. 
	 * @param quarto is the current state of the game.
	 * @param pDifficulty is the difficulty level of the AI.
	 *************************************************************************/
	public MiniMaxTree(final QuartoLogic quarto, final int pDifficulty) {
		
		this.difficulty = pDifficulty;
		this.head = quarto;
		
	}
	
	/**************************************************************************
	 * Build the MiniMax tree using calls to the recursive _buildTree method.
	 * Returns the move that generates the best score for current player.
	 * If no moves are available, returns [-1, -1, -1].
	 * In order to prevent excessive wait times, higher difficulties (above 5)
	 * get cut off during the first 5 moves of the game.
	 * @return an array representing the move to be made. The array looks like
	 * represents: [row played, column played, index of selected piece]
	 *************************************************************************/
	public int[] buildTree() {
		
		if (head.isWinner() || head.isDraw() || difficulty == 0) {
			return new int[] {-1, -1, -1};
		}
		
		int[] move = new int[]{-1, -1, -1};
		int height = difficulty;
		ArrayList<List<Integer>> emptyCells = head.getBoard().getEmptyCells();
		int numEmpty = emptyCells.get(0).size();
		if (numEmpty > FOURTEEN) {
			if (height > THREE) {
				height = THREE;
			}
		}
		if (numEmpty > TWELVE) {
			if (height > FOUR) {
				height = FOUR;
			}
		}
		int alpha = NEGINF;
		int beta = POSINF;
		for (int i = 0; i < numEmpty; ++i) {
			for (int index: head.getAvailableIndexes()) {
				int row = emptyCells.get(0).get(i);
				int col = emptyCells.get(1).get(i);
				QuartoLogic temp = new QuartoLogic(head);
				temp.placePiece(row, col);
				temp.selectPiece(index);
				if (temp.isWinner()) {
					//System.out.println("Making a winning move.");
					return new int[] {row, col, index};
				} else {
					if (temp.isDraw()) {
						alpha = temp.evaluate();
						if (alpha > beta) {
							//System.out.println(
									//"Found new best move: " + alpha);
							move = new int[] {row, col, index}; 
						}
					} else {
						int score = rBuildTree(
								temp, height - 1, alpha, beta, false);
						//System.out.println("Score: " + score);
						if (score > alpha) {
							alpha = score;
							//System.out.println(
							//		"Found new best move: " + alpha);
							move = new int[] {row, col, index};
							if (alpha > beta) {
								//System.out.println(
										//"Found best possible move: " + alpha);
								return move;
							}
						}
					}
				}
			}
		}
		//System.out.println("Giving best move.");
		return move;
		
	}
	
	/*************************************************************************
	 * The recursive call that builds the next layer of the miniMax tree from 
	 * a given node. This call implements alpha beta pruning to reduce the time
	 * the tree takes to find the best possible move. Only builds the tree to
	 * height - 1 nodes beneath this one.
	 * @param node is the Quarto Logic state from which the tree is being 
	 * built.
	 * @param height is the current height of the tree (height is 0 at the 
	 * leaves, maximum at the head node).
	 * @param alpha is the alpha value for alpha beta pruning.
	 * @param beta is the beta value for alpha beta pruning.
	 * @param max is true if this is a maximizing node, false if it a 
	 * minimizing node.
	 * @return the highest (or lowest) possible evaluation of the game board 
	 * based on the available moves.
	 *************************************************************************/
	private int rBuildTree(
			final QuartoLogic node, 
			final int height, 
			int alpha, int beta,  
			final boolean max) {
		
		// If node is leaf, evaluate node
		if (node.isWinner() || node.isDraw() || height == 0) {
			return evaluate(node, !max);
		}
		ArrayList<List<Integer>> emptyCells = node.getBoard().getEmptyCells();
		int numEmpty = emptyCells.get(0).size();
		for (int i = 0; i < numEmpty; ++i) {
			for (int index: node.getAvailableIndexes()) {
				int row = emptyCells.get(0).get(i);
				int col = emptyCells.get(1).get(i);
				QuartoLogic temp = new QuartoLogic(node);
				temp.placePiece(row, col);
				if (!temp.selectPiece(index)) {
					System.err.println(
							"Something went very wrong at height: " + height);
				}
				int score = rBuildTree(
						temp, height - 1, alpha, beta, !max);
				if (max) {
					if (score > alpha) {
						alpha = score;
					}
					if (alpha >= beta) {
						return alpha;
					}
				} else {
					if (score < beta) {
						beta = score;
					}
					if (alpha >= beta) {
						return beta;
					}
				}
			}
		}
		if (max) {
			return alpha;
		} else {
			return beta;
		}
		
	}
	
	/*************************************************************************
	 * Evaluates the given node from the point of view of the maximizing node.
	 * @param node is the node to be evaluated.
	 * @param max is true if the node is to be evaluated as a maximizing node, 
	 * false otherwise.
	 * @return an integer between -1000 and 1000, indicating the board's 
	 * evaluation from the point of view of the maximizing node.
	 */
	private int evaluate(final QuartoLogic node, final boolean max) {
		if (max) {
			if (node.isWinner() || node.isDraw()) {
				return node.evaluate();
			} else {
				return -node.evaluate();
			}
		} else {
			if (node.isDraw()) {
				return node.evaluate();
			} else {
				return -node.evaluate();
			}
		}
	}
	
}
