package quarto;

import java.io.Serializable;

/*************************************************************************
 * The Piece class represents a piece on the Quarto game board. Each piece
 * contains an icon, as well as 4 properties to differentiate itself from
 * the other pieces.
 * @author Frank
 * @since 09/17/2016
 ************************************************************************/
public final class Piece implements Serializable {

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** States whether the piece is hollow or not. */
	private Hollow hollow;
	
	/** States whether the piece is blue or black. */
	private Color color;
	
	/** States whether the piece is circular or square. */
	private Shape shape;
	
	/** States whether the piece is large or small. */
	private Size size;
	
	/*********************************************************************
	 * Instantiates a Quarto Piece with the given attributes, which will
	 * be represented on the game board with the given icon.
	 * @param pHollow is a Hollow enum that determines whether the Piece is 
	 * hollow or solid
	 * @param pColor is a Color enum that determines whether the Piece is 
	 * blue or black
	 * @param pShape is a Shape enum that determines whether the Piece is 
	 * round or square
	 * @param pSize is a Size enum that determines whether the Piece is 
	 * small or big
	 ********************************************************************/
	public Piece(final Hollow pHollow, final Color pColor, final Shape pShape, 
			final Size pSize) {
		
		this.hollow = pHollow;
		this.color = pColor;
		this.shape = pShape;
		this.size = pSize;
		
	}

	/**
	 * Returns whether the piece is hollow or not.
	 * @return Hollow.HOLLOW if hollow, or Hollow.SOLID if not
	 */
	public Hollow getHollow() {
		return hollow;
	}

	/**
	 * Sets the piece to the given value of hollow.
	 * @param pHollow is either Hollow.SOLID or Hollow.HOLLOW
	 */
	public void setHollow(final Hollow pHollow) {
		this.hollow = pHollow;
	}

	/**
	 * Returns the color of the piece.
	 * @return Color.BLUE if it's blue, Color.BLACK if it's black
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets the color of the piece to the given value.
	 * @param pColor is either Color.BLUE or Color.BLACK
	 */
	public void setColor(final Color pColor) {
		this.color = pColor;
	}

	/**
	 * Returns the shape of the piece.
	 * @return Shape.SQUARE if square, or Shape.ROUND if round
	 */
	public Shape getShape() {
		return shape;
	}

	/**
	 * Sets the shape of the piece to the given value.
	 * @param pShape is either Shape.SQUARE or Shape.ROUND
	 */
	public void setShape(final Shape pShape) {
		this.shape = pShape;
	}

	/**
	 * Returns the size of the piece.
	 * @return Size.SMALL if it's small, or Size.BIG if it's big
	 */
	public Size getSize() {
		return size;
	}

	/**
	 * Sets the size of the piece to the given value.
	 * @param pSize is either Size.SMALL or Size.BIG
	 */
	public void setSize(final Size pSize) {
		this.size = pSize;
	}
	
	/********************************************************************
	 * Retrieves a String summary of a single Quarto game piece.
	 * @return a 4-letter summary of the Quarto piece, with each letter
	 * standing for a particular feature.
	 * H = Hollow; S = Solid; B = Blue; L = Black; R = Round; Q = Square;
	 * G = Big; M = Small;
	 *******************************************************************/
	public String summary() {
		String summary = "";
		if (hollow == Hollow.HOLLOW) {
			summary += "H"; //Hollow
		} else {
			summary += "S"; //Solid
		}
		if (color == Color.BLUE) {
			summary += "B"; //Blue
		} else {
			summary += "L"; //Black
		}
		if (shape == Shape.ROUND) {
			summary += "R"; //Round
		} else {
			summary += "Q"; //Square
		}
		if (size == Size.BIG) {
			summary += "G"; //Big
		} else {
			summary += "M"; //Small
		}
		return summary;
	}
	
	/**********************************************************************
	 * Convert the Piece object to a String that represents the state of 
	 * the class.
	 * @return a String object that contains the parameters of this piece
	 ***********************************************************************/
	public String toString() {
		
		String hollowStr = "Solid";
		String colorStr = "Blue";
		String shapeStr = "Square";
		String sizeStr = "Small";
		if (this.hollow.equals(Hollow.HOLLOW)) {
			hollowStr = "Hollow";
		}
		if (this.color.equals(Color.BLACK)) {
			colorStr = "Black";
		}
		if (this.shape.equals(Shape.ROUND)) {
			shapeStr = "Round";
		}
		if (this.size.equals(Size.BIG)) {
			sizeStr = "Big";
		}
		return hollowStr + ", " + colorStr + ", " + shapeStr + ", " + sizeStr;
		
	}
	
	/**************************************************************************
	 * Checks whether this Piece is equal to another Piece by looking at the 
	 * Strings produced by their toString() methods.
	 * @param other is the other Piece object being compared to this Piece
	 * @return true if they are equal, false otherwise
	 *************************************************************************/
	public boolean equals(final Piece other) {
		
		if (other == null) {
			return false;
		} else {
			return this.toString().equals(other.toString());
		}
		
	}
	
	/**************************************************************************
	 * Checks whether this Piece is equal to another object by looking at the 
	 * Strings produced by their toString() methods.
	 * @param pOther is the other object being compared to this Piece
	 * @return true if they are equal, false otherwise
	 *************************************************************************/
	public boolean equals(final Object pOther) {
		
		if (pOther == null) {
			return false;
		} else {
			return (pOther instanceof Piece
					&& this.toString().equals(((Piece) pOther).toString()));
		}
		
	}
	
}
