package quarto;

import static org.junit.Assert.*;
import org.junit.Test;

/**********************
 * Test the Cell class.
 * @author Kyle Hekhuis
 * @since 10/20/16
 *********************/
public final class CellTest {

	/*******************************************************
	 * Test constructor Cell(boolean occupied, Piece piece)
	 * as well as getters.
	 ******************************************************/
	@Test
	public void constructorOneTest() {
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		Cell testCell = new Cell(true, testPiece);
		assertEquals(testCell.isOccupied(), true);
		assertEquals(testCell.getPiece(), testPiece);
	}
	
	/*********************************************
	 * Test constructor Cell() as well as getters.
	 ********************************************/
	@Test
	public void constructorTwoTest() {
		Cell testCell = new Cell();
		assertEquals(testCell.isOccupied(), false);
		assertEquals(testCell.getPiece(), null);
	}
	
	/********************************************
	 * Test setOccupied(boolean occupied) method.
	 *******************************************/
	@Test
	public void setOccupiedTest() {
		Cell testCell = new Cell();
		testCell.setOccupied(true);
		assertEquals(testCell.isOccupied(), true);
	}
	
	/************************************
	 * Test setPiece(Piece piece) method.
	 ***********************************/
	@Test
	public void setPieceTest() {
		Cell testCell = new Cell();
		Piece testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		testCell.setPiece(testPiece);
		assertEquals(testCell.getPiece(), testPiece);
	}
}
