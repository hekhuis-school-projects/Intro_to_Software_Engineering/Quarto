package quarto;
import java.awt.Toolkit;

import javax.swing.JFrame;

/***************************************************
 * The main entry point to the game.
 * Starts the options panel and instantiates a game
 * @author Gloire Rubambiza
 * @since 12/03/2016
 ***************************************************/
public class Main extends JFrame {
	
	 /** Added this to stop the editor from complaining. */
	private static final long serialVersionUID = 1L;

	/**Gets the screen size width. */
	private static final double WIDTH = 
			Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	
	/**Gets the screen size height. */
	private static final double HEIGHT = 
			Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	
	/*************************************
	 * Constructor that starts the Quarto game.
	 *******************************************/
	public Main() {
		super();
		add(new StartGame(this));
		setSize((int) WIDTH, (int) HEIGHT);
		setVisible(true);
	}
	
	/********************************************
	 * Main method that runs the Game.
	 * @param args are command line arguments passed to the function
	 **********************************************************/
	public static void main(final String[] args) {
		new Main();
	}
	
}
