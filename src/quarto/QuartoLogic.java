package quarto;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

/**********************************************************************
 * The Quarto class contains the logic and implementation of
 * the Quarto game. Creates the Quarto game which includes a board,
 * 16 pieces, two players, and player scores. Has the ability to 
 * determine if the game has been won, to place a piece on the board,
 * instantiate a new game and clear the board, advance the player
 * turn, and to undo moves.
 * @author Kyle Hekhuis
 * @since 10/03/16
 ***********************************************************************/
public final class QuartoLogic implements Serializable  {

	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

	/** Multiplier used for scoring by AI. */
	private static final int SCORE_MULTIPLIER = 100;
	
	/** Win score used for scoring by AI. */
	private static final int WIN_SCORE = 1000;
	
	/** Draw score used for scoring by AI. */
	private static final int DRAW_SCORE = 500;
	
	/** Row size of Quarto game board. */
	private static final int BOARD_ROWS = 4;
	
	/** Column size of Quarto game board. */
	private static final int BOARD_COLUMNS = 4;
	
	/** Number of diagonals on Quarto game board. */
	private static final int BOARD_DIAGONALS = 2;
	
	/** Used to check if diagonal from bottom left to top right. */
	private static final int DIAGONAL_CHECK = 3;
	
	/***************************************************************************
	 * NUM_OF_ATTRIBUTES is amount of counters for all the attributes for Piece.
	 * 1. Hollow: Hollow / Solid
	 * 2. Color: Blue / Black
	 * 3. Shape: Round / Square
	 * 4. Size: Big / Small.
	 **************************************************************************/
	private static final int NUM_OF_ATTRIBUTES = 4;
	
	/** How many game states can be undone. */
	private static final int AMOUNT_OF_UNDOS = 5;
	
	/** Quarto game board. */
	private Board board;
	
	/** Player entity which can either be a real person or AI. */
	private Player player1, player2;
	
	/** Current player whose turn it is. */
	private Player currentPlayer;
	
	/***********************************************************************
	 * Instantiates counters to keep track of 'score' for each row, column
	 * and diagonal. When a counter reaches +4 or -4, then the player who 
	 * placed the piece has won. If there are no more empty spaces on the
	 * board, and none of the counters is at +4 or -4, then the game ends
	 * in a draw.
	 **********************************************************************/
	
	/*************************************
	 * Counters for each row and property. 
	 * row| hollow | color | shape | size
	 *  0 |    0       0       0      0
	 *  1 |    0       0       0      0  
	 *  2 |    0       0       0      0
	 *  3 |    0       0       0      0
	 ************************************/
	private int[][] counterRows;
	
	/***************************************
	 * Counters for each column and property. 
	 * row| hollow | color | shape | size
	 *  0 |    0       0       0      0
	 *  1 |    0       0       0      0  
	 *  2 |    0       0       0      0
	 *  3 |    0       0       0      0
	 **************************************/
	private int[][] counterColumns;
	
	/******************************************
	 * Counters for each diagonal and property. 
	 * diagonal | hollow | color | shape | size
	 *     0    |    0       0       0      0
	 *     1    |    0       0       0      0  
	 *****************************************/
	private int[][] counterDiagonals;

	/** ArrayList of Piece containing all the available pieces. */
	private ArrayList<Piece> availablePieces;
	
	/** The currently selected Piece. */
	private Piece selectedPiece;
	
	/** Keeps track of whether or not there is a winner in the game. */
	private boolean isWinner = false;
	
	/** Keeps track of whether or not there is a draw in the game. */
	private boolean isDraw = false;
	
	/*********************************************************
	 * Array containing the current score for each player. 
	 * Index 1 = Player 1; Index 2 = Player 2 
	 ********************************************************/
	private int[] score;
	
	/********************************************
	 * Stack containing previous game states to
	 * revert back to for undo function. 
	 ********************************************/
	private MaxSizeStack<GameState> undoStack;
	
	/** A flag for telling whether the game is in the first turn or not. */
	private boolean firstTurn = true;
	
	/**
	 * The highest counter score - used to score board for the purposes of the 
	 * AI. 
	 */
	private int boardScore = 0;
	
	/********************************************************
	 * Instantiates a game of Quarto containing a 4x4 board,
	 * 16 pieces, 2 players, and player scores.
	 * @param pPlayer1 player 1 in the game
	 * @param pPlayer2 player 2 in the game
	 *******************************************************/
	public QuartoLogic(final Player pPlayer1, final Player pPlayer2) {
		this.player1 = pPlayer1;
		this.player2 = pPlayer2;
		currentPlayer = player1;
		board = new Board(BOARD_ROWS, BOARD_COLUMNS);
		instantiateCounters();
		instantiateScore();
		instantiatePieces();
		undoStack = new MaxSizeStack<GameState>(AMOUNT_OF_UNDOS);
		saveGameState();
	}
	
	/**************************************************************************
	 * Constructs a new QuartoLogic class from an existing one. Used mostly
	 * in the MinMaxTree.
	 * @param other is the other QuartoLogic class from which a new one is being
	 * constructed.
	 *************************************************************************/
	public QuartoLogic(final QuartoLogic other) {
		this.board = new Board(other.board);
		this.player1 = other.player1;
		this.player2 = other.player2;
		this.currentPlayer = other.currentPlayer;
		this.counterRows = cloneCounter(other.counterRows);
		this.counterColumns = cloneCounter(other.counterColumns);
		this.counterDiagonals = cloneCounter(other.counterDiagonals);
		this.availablePieces = other.cloneAvailablePieces();
		this.selectedPiece = other.cloneSelectedPiece();
		this.isWinner = other.isWinner;
		this.isDraw = other.isDraw;
		this.score = other.cloneScore();
		// this.undoStack = other.undoStack; // Not needed for MinMaxTree
		this.firstTurn = other.firstTurn;
	}
	
	/********************************************************************
	 * Fills ArrayList<Piece> availablePieces with all 16 Quarto pieces.
	 *******************************************************************/
	private void instantiatePieces() {
		availablePieces = new ArrayList<Piece>();
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.SQUARE, 
						Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLACK, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLACK, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.SQUARE, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.SQUARE, Size.SMALL));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.ROUND, Size.BIG));
		availablePieces.add(
				new Piece(Hollow.SOLID, Color.BLUE, Shape.ROUND, Size.SMALL));
	}
	
	/**********************************************
	 * Instantiates all the counters and sets each
	 * entry in the counters to 0.
	 *********************************************/
	private void instantiateCounters() {
		counterRows = new int[BOARD_ROWS][NUM_OF_ATTRIBUTES];
		counterColumns = new int[BOARD_COLUMNS][NUM_OF_ATTRIBUTES];
		counterDiagonals = new int[BOARD_DIAGONALS][NUM_OF_ATTRIBUTES];
	}
	
	/****************************************************************
	 * Instantiates the player score to 0 for both players.
	 ***************************************************************/
	private void instantiateScore() {
		score = new int[] {0, 0};
	}
	
	/**********************************************************************
	 * Increments the value of a single counter, and changes the value
	 * of isWinner if a winner occurs.
	 * @param counters is the single counter to be incremented
	 * @param row is the row to increment
	 * @param property is the property of the piece
	 *********************************************************************/
	private void inc(int[][] counters, final int row, final int property) {
		final int incWinningCount = 4;
		counters[row][property]++;
		if (counters[row][property] == incWinningCount) {
			isWinner = true;
			setScore();
		}
		if (counters[row][property] > boardScore) {
			boardScore = counters[row][property];
		}
		//setDraw();
	}
	
	/**********************************************************************
	 * Decrements the value of a single counter, and changes the value
	 * of isWinner if a winner occurs.
	 * @param counters is the single counter to be decremented
	 * @param row is the row to increment
	 * @param property is the property of the piece
	 *********************************************************************/
	private void dec(int[][] counters, final int row, final int property) {
		final int decWinningCount = -4;
		counters[row][property]--;
		if (counters[row][property] == decWinningCount) {
			isWinner = true;
			setScore();
		}
		if (Math.abs(counters[row][property]) > boardScore) {
			boardScore = Math.abs(counters[row][property]);
		}
		//setDraw();
	}
	
	/**********************************************************************
	 * Updates all the relevant counters to currently played piece, and 
	 * changes the value of isWinner if the piece played is the winning
	 * piece.
	 * @param row is the row of the cell onto which the Piece was played
	 * @param col is the col of the cell onto which the Piece was played
	 **********************************************************************/
	private void updateCounters(final int row, final int col) {
		final int property1 = 0;
		final int property2 = 1;
		final int property3 = 2;
		final int property4 = 3;
		
		//Update Hollow
		if (selectedPiece.getHollow() == Hollow.HOLLOW) {
			inc(counterRows, row, property1);
			inc(counterColumns, col, property1);
			if (row == col) {
				inc(counterDiagonals, 0, property1);
			}
			if (row + col == DIAGONAL_CHECK) {
				inc(counterDiagonals, 1, property1);
			}
		} else {
			dec(counterRows, row, property1);
			dec(counterColumns, col, property1);
			if (row == col) {
				dec(counterDiagonals, 0, property1);
			}
			if (row + col == DIAGONAL_CHECK) {
				dec(counterDiagonals, 1, property1);
			}
		}
		//Update Color
		if (selectedPiece.getColor() == Color.BLUE) {
			inc(counterRows, row, property2);
			inc(counterColumns, col, property2);
			if (row == col) {
				inc(counterDiagonals, 0, property2);
			}
			if (row + col == DIAGONAL_CHECK) {
				inc(counterDiagonals, 1, property2);
			}
		} else {
			dec(counterRows, row, property2);
			dec(counterColumns, col, property2);
			if (row == col) {
				dec(counterDiagonals, 0, property2);
			}
			if (row + col == DIAGONAL_CHECK) {
				dec(counterDiagonals, 1, property2);
			}
		}
		//Update Shape
		if (selectedPiece.getShape() == Shape.ROUND) {
			inc(counterRows, row, property3);
			inc(counterColumns, col, property3);
			if (row == col) {
				inc(counterDiagonals, 0, property3);
			}
			if (row + col == DIAGONAL_CHECK) {
				inc(counterDiagonals, 1, property3);
			}
		} else {
			dec(counterRows, row, property3);
			dec(counterColumns, col, property3);
			if (row == col) {
				dec(counterDiagonals, 0, property3);
			}
			if (row + col == DIAGONAL_CHECK) {
				dec(counterDiagonals, 1, property3);
			}
		}
		//Update Size
		if (selectedPiece.getSize() == Size.BIG) {
			inc(counterRows, row, property4);
			inc(counterColumns, col, property4);
			if (row == col) {
				inc(counterDiagonals, 0, property4);
			}
			if (row + col == DIAGONAL_CHECK) {
				inc(counterDiagonals, 1, property4);
			}
		} else {
			dec(counterRows, row, property4);
			dec(counterColumns, col, property4);
			if (row == col) {
				dec(counterDiagonals, 0, property4);
			}
			if (row + col == DIAGONAL_CHECK) {
				dec(counterDiagonals, 1, property4);
			}
		}
	}
	
	/************************************************
	 * Places a piece on a certain spot on the board.
	 * @param piece piece to be placed on board
	 * @param row row to place the piece into
	 * @param column column to place the piece into
	 * @return true if piece was placed successfully
	 ***********************************************/
	public boolean placePiece(final Piece piece, final int row, 
			final int column) {
		return board.placePiece(piece, row, column);
	}
	
	/***********************************************************************
	 * Places selected piece onto the empty board cell, takes the piece out of
	 * available pieces, and updates the counters.
	 * @param row is the row of the cell onto which the Piece was played
	 * @param col is the column of the cell onto which the Piece was played
	 * @return true if placing the piece succeeded, false otherwise
	 *********************************************************************/
	public boolean placePiece(final int row, final int col) {
		boolean success = board.placePiece(selectedPiece, row, col);
		if (success) {
			updateCounters(row, col);
			selectedPiece = null;
			setDraw();
			if (isDraw) {
				System.out.println("Now there is a draw in the game!");
			}
			return true;
		} else {
			//System.out.println("Couldn't place piece: " + row + " " + col);
			return false;
		}
	}
	
	/*************************************************************************
	 * Selects given piece, removes selected piece from availablePieces 
	 * ArrayList because it can no longer be played.
	 * @param index is the index of the selected Piece in the availablePieces
	 * ArrayList
	 * @return true if operations is successful, false otherwise
	 ************************************************************************/
	public boolean selectPiece(final int index) {
		//undoStack.push(createGameState());
		if (availablePieces.get(index) == null) {
			System.err.println("Attempting to select null piece.");
			return false;
		}
		this.selectedPiece = new Piece(availablePieces.get(index).getHollow(),
									   availablePieces.get(index).getColor(),
									   availablePieces.get(index).getShape(),
									   availablePieces.get(index).getSize());
		availablePieces.set(index,  null);
		return true;
	}
	
	/**************************************************************************
	 * If current player is an AI, makes a move using the AI of the player.
	 *************************************************************************/
	public void moveAI() {
	
		if (!currentPlayer.isAI()) {
			return; // Do nothing if current player is not an AI
		}
		// If it's the first turn, AI selects piece for next opponent to play
		if (firstTurn) { 
			int index = currentPlayer.selectPiece(availablePieces);
			while (availablePieces.get(index) == null) {
				index = currentPlayer.selectPiece(availablePieces);
			}
			selectPiece(index);
			saveGameState();
		} else {
			MiniMaxTree miniMaxTree = new MiniMaxTree(
					new QuartoLogic(this), currentPlayer.getDifficulty());
			int[] move = miniMaxTree.buildTree();
			placePiece(move[0], move[1]);
			// Stop move if the game is over
			if (isWinner || isDraw) { 
				return;
			}
			if (!selectPiece(move[2])) {
				System.err.println("Selecting piece at index: " + move[2] 
						+ " failed.");
			}
			saveGameState();
		}
		
	}
	
	/************************************
	 * Clears all pieces from the board.
	 ***********************************/
	public void clearBoard() {
		board.clearBoard();
	}
	
	/********************************************************************
	 * Returns the current state of the game board.
	 * @return the Board object used by the current game
	 * @author Frank
	 ********************************************************************/
	public Board getBoard() {
		return this.board;	
	}
	
	/*****************************************
	 * Saves a game state to the undo stack.
	 **********************************************/
	public void saveGameState() {
		undoStack.push(createGameState());
		System.out.println("Made a move, size of stack: " + undoStack.size());
	}
	
	/**************************************************************************
	 * Undoes the last move.
	 * @return true if undo stack has at least one game state, false otherwise
	 *************************************************************************/
	public boolean undoMove() {
		if (!undoStack.isEmpty()) {
			GameState temp = undoStack.pop();
			board = temp.getBoard();
			currentPlayer = temp.getCurrentPlayer();
			//moveAI();
			counterRows = temp.getCounterRows();
			counterColumns = temp.getCounterColumns();
			counterDiagonals = temp.getCounterDiagonals();
			availablePieces = temp.getAvailablePieces();
			score = temp.getScore();
			selectedPiece = temp.getSelectedPiece();
			System.out.println("Undid a move, size of stack: " 
					+ undoStack.size());
			if (undoStack.isEmpty()) {
				saveGameState();
			}
			return true;
		}
		return false;
	}
	
	/***************************************************
	 * Gets the current game state and returns it.
	 * @return GameState object with current game state
	 **************************************************/
	private GameState createGameState() {
		return new GameState(board.cloneBoard(),
							 player1.clonePlayer(),
							 player2.clonePlayer(),
							 currentPlayer.clonePlayer(), 
							 cloneCounter(counterRows), 
							 cloneCounter(counterColumns), 
							 cloneCounter(counterDiagonals), 
							 cloneAvailablePieces(), 
							 cloneScore(),
							 cloneSelectedPiece());
	}
	
	/*************************************
	 * Clones the current availablePieces.
	 * @return clone of availablePieces
	 *************************************/
	private ArrayList<Piece> cloneAvailablePieces() {
		ArrayList<Piece> clone = new ArrayList<Piece>();
		for (Piece p : availablePieces) {
			clone.add(p);
		}
		return clone;
	}
	
	/****************************************
	 * Clones the passed counter.
	 * @param counter counter to clone
	 * @return cloned copy of passed counter
	 ***************************************/
	private int[][] cloneCounter(final int[][] counter) {
		int[][] clone = new int[counter.length][counter[0].length];
		for (int i = 0; i < clone.length; ++i) {
			for (int j = 0; j < clone[i].length; ++j) {
				clone[i][j] = counter[i][j];
			}
		}
		return clone;
	}
	
	/*********************************
	 * Clones the current score.
	 * @return clone of current score
	 *********************************/
	private int[] cloneScore() {
		int[] clone = new int[2];
		clone[0] = score[0];
		clone[1] = score[1];
		return clone;
	}
	
	/******************************************
	 * Clones the current selected piece.
	 * @return clone of current selected piece
	 ******************************************/
	private Piece cloneSelectedPiece() {

		if (selectedPiece == null) {
			return null;
		}
		return new Piece(selectedPiece.getHollow(),
						 selectedPiece.getColor(),
						 selectedPiece.getShape(),
						 selectedPiece.getSize());
	}
	
	/********************************************************************
	 * Checks to see if the game has been won.
	 * @return true if winning conditions have been met, false otherwise.
	 *******************************************************************/
	public boolean isWinner() {
		return isWinner;
	}
	
	/************************************************************
	 * Checks to see if game has ended in a draw.
	 * @return true if game has ended in a draw, false otherwise.
	 ***********************************************************/
	public boolean isDraw() {
		return isDraw;
	}
	
	/***************************************
	 * Advances the turn to the next player.
	 **************************************/
	public void nextTurn() {
		if (currentPlayer.equals(player1)) {
			currentPlayer = player2;
		} else {
			currentPlayer = player1;
		}
		firstTurn = false;
	}
	
	/**************************************************
	 * Checks if the board is full. If it is, the game
	 * is a draw and sets isDraw to true.
	 *************************************************/
	private void setDraw() {
		if (!isWinner && board.isFull()) {
			isDraw = true;
		}
	}
	
	/*************************************************************************
	 * Starts a new round, keeping settings.
	 *************************************************************************/
	public void nextRound() {
		board.clearBoard();
		instantiatePieces();
		instantiateCounters();
		isWinner = false;
		isDraw = false;
		firstTurn = true;
		undoStack.clear();
		saveGameState();
	}
	
	/*****************************************************
	 * Sets the players' score based off who won the game.
	 ****************************************************/
	private void setScore() {
		if (isWinner) {
			if (currentPlayer.equals(player1)) {
				score[0]++;
			} else {
				score[1]++;
			}
		}
	}
	
	/**************************************************
	 * Returns an array containing each player's score.
	 * @return int,  containing each player's score.
	 *************************************************/
	public int[] getScore() {
		int[] temp = this.score;
		return temp;
	}
	
	/****************************************************
	 * Retrieves the pieces that have not been used yet.
	 * @return an ArrayList<Piece> of all the pieces that
	 * have not yet been used by the game.
	 ***************************************************/
	public ArrayList<Piece> getPieces() {
		return availablePieces;
	}
	
	/**************************************************
	 * Retrieves the player who's turn it is currently.
	 * @return the Player whose turn it is
	 *************************************************/
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}
	
	/****************************************************
	 * Retrieves the currently selected piece.
	 * @return null if no piece is currently selected, or
	 * the currently selected Piece object
	 ***************************************************/
	public Piece getSelected() {
		return selectedPiece;
	}
	
	/*******************************************************
	 * Tells whether the game is in its first turn or not.
	 * @return True when it is in the first turn, false otherwise
	 ********************************************************/
	public boolean getFirstTurn() {
		return this.firstTurn;
	}
	
	/**************************************************************
	 * Returns the player 1 object.
	 * @return player1
	 ***************************************************************/
	public Player getPlayer1() {
		return this.player1;
	}
	
	/**************************************************************
	 * Returns the player 2 object.
	 * @return player2
	 ***************************************************************/
	public Player getPlayer2() {
		return this.player2;
	}
	
	/****************************************************************
	 * Obtains the indexes of the available Pieces. Goes through the 
	 * available Pieces array, and adds the indexes where available
	 * Piece is not null to a list object that is then returned.
	 * @return a List of indexes of available Pieces.
	 ****************************************************************/
	public List<Integer> getAvailableIndexes() {
		List<Integer> availableIndexes = new ArrayList<Integer>();
		for (int i = 0; i < availablePieces.size(); ++i) {
			if (availablePieces.get(i) != null) {
				availableIndexes.add(i);
			}
		}
		return availableIndexes;
	}
	
	/****************************************************************
	 * Returns the score of the gameBoard. If there is a winner, 
	 * the score is 1000. If there is a draw, the score is 500. Else, 
	 * the score is the highest counter score * 100
	 * @return an integer representing the value of the Board.
	 ****************************************************************/
	public int evaluate() {
		if (isWinner()) {
			return WIN_SCORE;
		}
		if (isDraw()) {
			return DRAW_SCORE;
		}
		return boardScore * SCORE_MULTIPLIER;
	}
}