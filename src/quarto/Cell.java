package quarto;

import java.io.Serializable;

/*********************************************************
 * The Cell class represents a spot on a game board with
 * the information of whether the spot is currently
 * occupied and if it is, what piece is on that spot.
 * If there is no piece located in the spot, the value of
 * piece for the cell is null.
 * @author Kyle Hekhuis
 * @since 9/22/16
 ********************************************************/
public final class Cell implements Serializable {
		
	/** Objects of the class are now serializable. */
	private static final long serialVersionUID = 1L;

		/** Tells whether cell is occupied or not. True for yes. */
		private boolean occupied;
		
		/** Piece that is located in the cell. Null if none*/
		private Piece piece;

		/**************************************************
		 * Instantiates a cell with the given occupied and
		 * piece parameters.
		 * @param pOccupied true if spot is occupied
		 * @param pPiece piece that is occupying this spot
		 *************************************************/
		public Cell(final boolean pOccupied, final Piece pPiece) {
			this.occupied = pOccupied;
			this.piece = pPiece;
		}

		/***********************************************
		 * Instantiates a cell that is not occupied and
		 * has no piece occupying it.
		 **********************************************/
		public Cell() {
			occupied = false;
			piece = null;
		}

		/***********************************************
		 * Determine if the cell is currently occupied.
		 * @return true if cell is occupied
		 **********************************************/
		public boolean isOccupied() {
			return occupied;
		}

		/*******************************************
		 * Gets the piece occupying the cell.
		 * @return piece that is occupying the cell.
		 ******************************************/
		public Piece getPiece() {
			return piece;
		}

		/********************************************
		 * Sets the occupied status of the cell.
		 * @param pOccupied true if cell is occupied.
		 *******************************************/
		public void setOccupied(final boolean pOccupied) {
			this.occupied = pOccupied;
		}

		/*********************************************
		 * Sets the cell's piece with the given piece.
		 * @param pPiece piece to place in this cell
		 ********************************************/
		public void setPiece(final Piece pPiece) {
			this.piece = pPiece;
		}
}
