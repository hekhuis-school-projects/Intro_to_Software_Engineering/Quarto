package quarto;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/************************************************************************
 * Tests the constructor, summary, and toString methods of the Piece 
 * class 
 * @author Frank
 * @date 10/12/2016
 ************************************************************************/
public class PieceTest {

	private Piece testPiece;
	
	/*********************************************************************
	 * This method runs before every test, and instantiates a Piece that
	 * is Hollow, Blue, Round and Big.
	 ********************************************************************/
	@Before
	public void instantiatePiece() {
		testPiece = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
	}
	
	/*********************************************************************
	 * Tests constructor, to make sure that the initialization is done
	 * right.
	 ********************************************************************/
	@Test
	public void constructorTest() {
		assertEquals(testPiece.getHollow(), Hollow.HOLLOW);
		assertNotEquals(testPiece.getHollow(), Hollow.SOLID);
		assertEquals(testPiece.getColor(), Color.BLUE);
		assertNotEquals(testPiece.getColor(), Color.BLACK);
		assertEquals(testPiece.getShape(), Shape.ROUND);
		assertNotEquals(testPiece.getShape(), Shape.SQUARE);
		assertEquals(testPiece.getSize(), Size.BIG);
		assertNotEquals(testPiece.getSize(), Size.SMALL);
	}
	
	/*********************************************************************
	 * Tests the toString method.
	 ********************************************************************/
	@Test
	public void toStringTest() {
		Hollow hollows[] = {Hollow.HOLLOW, Hollow.SOLID};
		Color colors[] = {Color.BLUE, Color.BLACK};
		Shape shapes[] = {Shape.ROUND, Shape.SQUARE};
		Size sizes[] = {Size.BIG, Size.SMALL};
		String hollowsStr[] = {"Hollow", "Solid"};
		String colorsStr[] = {"Blue", "Black"};
		String shapesStr[] = {"Round", "Square"};
		String sizesStr[] = {"Big", "Small"};
		String temp;
		for(int h = 0; h < 2; ++h) {
			testPiece.setHollow(hollows[h]);
			for(int c = 0; c < 2; ++c) {
				testPiece.setColor(colors[c]);
				for(int s = 0; s < 2; ++s) {
					testPiece.setShape(shapes[s]);
					for(int i = 0; i < 2; ++i) {
						testPiece.setSize(sizes[i]);
						temp = hollowsStr[h];
						temp += ", " + colorsStr[c];
						temp += ", " + shapesStr[s];
						temp += ", " + sizesStr[i];
						assertEquals(temp, testPiece.toString());
						//Need to add tests for ImageIcon
					}
				}
			}
		}
	}
	
	/*********************************************************************
	 * Tests the summary method.
	 ********************************************************************/
	@Test
	public void summaryTest() {
		Hollow hollows[] = {Hollow.HOLLOW, Hollow.SOLID};
		Color colors[] = {Color.BLUE, Color.BLACK};
		Shape shapes[] = {Shape.ROUND, Shape.SQUARE};
		Size sizes[] = {Size.BIG, Size.SMALL};
		String hollowsStr[] = {"H", "S"};
		String colorsStr[] = {"B", "L"};
		String shapesStr[] = {"R", "Q"};
		String sizesStr[] = {"G", "M"};
		String temp;
		for(int h = 0; h < 2; ++h) {
			testPiece.setHollow(hollows[h]);
			for(int c = 0; c < 2; ++c) {
				testPiece.setColor(colors[c]);
				for(int s = 0; s < 2; ++s) {
					testPiece.setShape(shapes[s]);
					for(int i = 0; i < 2; ++i) {
						testPiece.setSize(sizes[i]);
						temp = hollowsStr[h];
						temp += colorsStr[c];
						temp += shapesStr[s];
						temp += sizesStr[i];
						assertEquals(temp, testPiece.summary());
					}
				}
			}
		}
	}
	
	/**************************************************************************
	 * Tests the equals method with a null Piece
	 */
	@Test
	public void nullPieceEqualsTest() {
		assertFalse(testPiece.equals(null));
		Piece piece = null;
		assertFalse(testPiece.equals(piece));
		Object object = null;
		assertFalse(testPiece.equals(object));
	}
	
	/************************
	 * Tests the equals method with an Object that isn't a Piece
	 */
	@Test
	public void wrongInstanceEqualsTest() {
		assertFalse(testPiece.equals("Hello"));
		assertFalse(testPiece.equals(testPiece.toString()));
		assertFalse(testPiece.equals(new Player("Player")));
	}
	
	/**************************
	 * Tests the equals method with other Pieces
	 ****************/
	@Test
	public void validEqualsTest() {
		Piece piece2 = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		Piece piece3 = new Piece(
				Hollow.SOLID, Color.BLUE, Shape.ROUND, Size.BIG);
		Object object = new Piece(
				Hollow.HOLLOW, Color.BLUE, Shape.ROUND, Size.BIG);
		assertTrue(testPiece.equals(piece2));
		assertFalse(testPiece.equals(piece3));
		assertTrue(testPiece.equals(object));
		assertFalse(piece3.equals(object));
	}
	
}
